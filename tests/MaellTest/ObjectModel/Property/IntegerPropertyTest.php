<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\IntegerProperty;

/**
 * StringProperty test case.
 */
class IntegerPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var IntegerProperty
     */
    private $IntegerProperty;
    
    
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->IntegerProperty = new IntegerProperty('test');
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->IntegerProperty = null;
        parent::tearDown();
    }

    
    /**
     * Tests IntegerProperty->setValue() with an integer
     */
    public function testSetValueWithInteger()
    {
        $this->IntegerProperty->setValue(123);
        $this->assertEquals(123, $this->IntegerProperty->getValue());
    }
    

    /**
     * Tests IntegerProperty->setValue() with a numeric string
     */
    public function testSetValueWithNumericString()
    {
        $this->IntegerProperty->setValue("123");
        $this->assertEquals(123, $this->IntegerProperty->getValue());
    }
    
    
    /**
     * Tests IntegerProperty->setValue() with a non numeric string
     * @expectedException Maell\ObjectModel\Property\Exception
     */
    public function testSetValueWithNonNumericString()
    {
        $this->IntegerProperty->setValue("string");
    }
}
