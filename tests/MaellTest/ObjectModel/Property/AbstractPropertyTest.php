<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\StringProperty;
use Maell\ObjectModel\DataObject;

/**
 * StringProperty test case.
 */
class AbstractPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var StringProperty
     */
    private $StringProperty;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $params = ['defaultvalue' => 'none', 'label' => 'MyLabel', 'help' => 'Some Help'];
        $this->StringProperty = new StringProperty('test', $params);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->StringProperty = null;
        parent::tearDown();
    }

    
    public function testParentIsDataObject()
    {
        $this->StringProperty->setParent(new DataObject('Maell\Core\BaseUser'));
        $this->assertInstanceOf('Maell\ObjectModel\DataObject', $this->StringProperty->getParent());
    }
    
    
    public function testValueIsNullByDefault()
    {
        $this->assertNull($this->StringProperty->getValue());
    }
    

    public function testValueHasNotChanged()
    {
        $this->assertEquals(false, $this->StringProperty->hasChanged());
    }
    
    
    public function testDefaultValueIsSet()
    {
        $this->assertEquals('none', $this->StringProperty->getDefaultValue());
    }
    

    public function testLabelIsSet()
    {
        $this->assertEquals('MyLabel', $this->StringProperty->getLabel());
    }
    

    public function testSetNewLabel()
    {
        $this->StringProperty->setLabel('MyNewLabel');
        $this->assertEquals('MyNewLabel', $this->StringProperty->getLabel());
    }
    
    public function testSetMultipleLabels()
    {
        $this->StringProperty->setLabel(['fr' => 'MonLibellé', 'en' => 'MyLabel']);
        $this->assertEquals('MonLibellé', $this->StringProperty->getLabel('fr'));
        $this->assertEquals('MyLabel', $this->StringProperty->getLabel('en'));
    }

    
    /**
     * Tests StringProperty->setValue()
     */
    public function testSetValue()
    {
        $this->StringProperty->setValue('string');
        $this->assertEquals('string', $this->StringProperty->getValue());
    }
    
    
    public function testValueHasChanged()
    {
        $this->StringProperty->setValue('string');
        $this->assertEquals(true, $this->StringProperty->hasChanged());
    }

    
    public function testResetCallShouldSetValueWithInitialValue()
    {
        $this->StringProperty->setValue('string1');
        $this->StringProperty->reset();
        $this->assertEquals($this->StringProperty->getInitialValue(), $this->StringProperty->getValue());
    }
    
    
    public function testResetValue()
    {
        $this->StringProperty->setValue('string1');
        $this->StringProperty->resetValue();
        $this->assertNull($this->StringProperty->getValue());
    }
    
    
    public function testHelpTextIsDefined()
    {
        $this->assertEquals('Some Help', $this->StringProperty->getHelpText());
    }
}

