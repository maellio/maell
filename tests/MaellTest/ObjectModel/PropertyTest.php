<?php

namespace MaellTest\ObjectModel;

/**
 * Property test suite.
 */
class PropertyTest extends \PHPUnit_Framework_TestSuite
{

    /**
     * Constructs the test suite handler.
     */
    public function __construct()
    {      
        $this->setName('PropertySuite');

        $this->addTestSuite('\MaellTest\ObjectModel\Property\AbstractPropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\StringPropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\ArrayPropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\DatePropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\CurrencyPropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\IntegerPropertyTest');
        $this->addTestSuite('\MaellTest\ObjectModel\Property\FloatPropertyTest');
        
    }

    
    /**
     * Creates the suite.
     */
    public static function suite()
    {
        return new self();
    }
}
