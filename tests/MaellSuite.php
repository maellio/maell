<?php


/**
 * Static test suite.
 */
class MaellSuite extends \PHPUnit_Framework_TestSuite
{

    /**
     * Constructs the test suite handler.
     */
    public function __construct()
    {      
        $this->setName('MaellSuite');
        $this->addTestSuite('\MaellTest\ObjectModel\PropertyTest');
    }

    
    /**
     * Creates the suite.
     */
    public static function suite()
    {
        return new self();
    }
}

