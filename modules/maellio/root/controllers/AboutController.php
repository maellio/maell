<?php

use Maell\View;
use Maell as Maell;

/**
 * AboutController
 * 
 * @author
 * @version 
 */
require_once 'vendor/maellio/maell/controllers/DefaultController.php';

class Root_AboutController extends Maell_DefaultController {

	
	/**
	 * The default action - show the home page
	 */
	public function creditsAction()
	{
		$sc = new View\SimpleComponent();
		$sc->setTitle("Maell Credits");
		$sc->setContent(nl2br(file_get_contents(Maell::$basePath . 'vendor/maellio/maell/README')));
		$sc->register();
	}
	
	
	public function licenseAction()
	{
		$sc = new View\SimpleComponent();
		$sc->setTitle("Maell License");
		$sc->setContent(nl2br(file_get_contents(Maell::$basePath . 'vendor/maellio/maell/LICENSE.txt')));
		$sc->register();
	}
}
