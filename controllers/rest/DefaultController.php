<?php

/**
 * DefaultController
 * 
 * @author
 * @version 
 */

use Maell\Core\Registry;
use Maell\Core;
use Maell\ObjectModel;
use Maell\Backend;
use Maell\View\Action;

require_once 'Zend/Controller/Action.php';

class Rest_DefaultController extends Zend_Controller_Action {

	
	protected $_uuid;
	
	
	protected $_obj = null;
	
	
	protected $_status = 'OK';
	
	
	protected $_refreshCache = false;
	
	
	protected $_data = array();
	
	
	protected $_context = array();

	
	protected $_actions = array('pre' => array(), 'ok' => array(), 'nok' => array());
	
	
	protected $_redirects = array();
	
	
	protected $_post;
	
	
	public function init()
	{
		/* redeclare with your ACL in your own controller */
	}
	
	
	public function preDispatch()
	{
		$this->_post = $this->getRequest()->getPost();
		$this->_uuid = $this->_getParam('uuid');
	
		if ($this->_uuid) {
			$this->_obj = Core\Registry::get($this->_uuid);
			$this->_defineRedirect(array('ok','nok','err'));
	
			if ($this->_obj instanceof ObjectModel\ObjectUri) {
				$this->_obj = ObjectModel::factory($this->_obj);
			}
	
			if (! $this->_obj) {
				$this->_context['message'] = "Unable to restore object";
				$this->_status = 'ERR';
				$this->postDispatch();
			}

			// @todo post actions coming from js, unsecure, we should keep a reference of the handling form object
			if ($this->_getParam('post_ok')) {
				$this->_actions['ok'] = $this->_getParam('post_ok');
			}
			
		} else {
			$this->_context['message'] = 'Missing remote object id';
			$this->_status = 'NOK';
			$this->postDispatch();
		}
	}
	
	
	public function postDispatch()
	{
		if ($this->_uuid && $this->_refreshCache === true) {
			Registry::set($this->_obj, $this->_uuid, true);
		}
		
		// reinject some data 
		foreach ($this->_post as $key => $val) {
			if (substr($key,0,1) == '_') {
				$this->_data[$key] = $val;
			}
		}
		
		// if a redirect is available for the status, add it to the data
		if (isset($this->_redirects[strtolower($this->_status)])) {
			// declare object in tag parsing class to use it for tag substitution on redirect urls
			// ex: http://my.domain.com/redirect/to/%object:_identifier%
			if ($this->_obj instanceof ObjectModel\BaseObject || $this->_obj instanceof ObjectModel\DataObject) {
				Core\Tag\ObjectTag::$object = $this->_obj;
			} else if ($this->_obj instanceof Action\AbstractAction) {
				Core\Tag\ObjectTag::$object = $this->_obj->getObject();
			}
			$this->_context['redirect'] = Core\Tag::parse($this->_redirects[strtolower($this->_status)]);
		}
			
		if (isset($this->_post['_debug'])) {
		    $this->_context['debug'] = Backend::getLastQuery();
		}
		exit($this->_setResponse($this->_status, $this->_data, $this->_context));
	}
	
	
	protected function _setResponse($status = 'OK', $data, $context = null, $format = 'json')
	{
	    // flush log
	    // @see http://framework.zend.com/manual/1.9/en/zend.log.writers.html#zend.log.writers.firebug
	    $response = new Zend_Controller_Response_Http();
	    $channel = Zend_Wildfire_Channel_HttpHeaders::getInstance();
	    $channel->setRequest(new Zend_Controller_Request_Http());
	    $channel->setResponse($response);
	    $channel->flush();
	    $response->sendHeaders();
	    
		$response = array('status' => $status, 'data' => $data, 'context' => $context);
		
 		switch ($format) {
 			
 			case 'json':
 			default:
 				header('Content-Type:application/json');
 				if (\Maell::getEnvData('cache_datasets') !== false) {
	 				header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
 					header_remove('Cache-Control');
 					header_remove('Pragma');
 				}
 				return \Zend_Json::encode($response);
 				break;
 		}
		
	}
	
	
	/**
	 * Define the various redirects base on POST data or object parameters
	 * @param mixed $val
	 */
	protected function _defineRedirect($val)
	{
		if (! is_array($val)) {
		    $val = (array) $val;
		}
		
		foreach ($val as $key) {
			$var = 'redirect_' . $key;
			if (isset($this->_post[$var])) {
				$this->_redirects[$key] = $this->_post[$var];
			} else if ($this->_obj instanceof Action\AbstractAction && $this->_obj->getParameter($var)) {
				$this->_redirects[$key] = $this->_obj->getParameter($var);
			}
		}
	}
}
