<?php

use Maell\View\FormComponent;
use Maell\ObjectModel\ObjectUri;
use Maell\View;
use Maell\View\ListComponent;
use Maell\ObjectModel\Collection;


require_once 'DefaultController.php';

abstract class Maell_CrudController extends Maell_DefaultController {


	protected $_class;
	
	
	public function init()
	{
		parent::init();
		//View::addRequiredLib('http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', 'js');
		//View::addRequiredLib('http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', 'js');
		//View::addRequiredLib('http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css','css');
		//View::addCoreLib('core.js');
	}
	
	
	public function indexAction()
	{
		
	}

	
	public function readAction()
	{
		$collection = new Collection($this->_class);
		
		if ($this->_getParam('id')) {
			$collection->having(ObjectUri::IDENTIFIER)->equals($this->_getParam('id'));
			$collection->setBoundaryBatch(1)->find();
			if ($collection->getTotalMembers() == 1) {
				$form = new FormComponent($collection->getMember(Collection::POS_FIRST));
				$form->register();
			}
		} else {
			
			$list = new ListComponent($collection);
			$list->addRowAction($_SERVER['REQUEST_URI'], 'Read', array('icon' => 'tool-blue'));
			$list->register();
		}
	}
}

