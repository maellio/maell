<?php

/**
 * DefaultController
 * 
 * @author
 * @version 
 */

use Maell\Core;

require_once 'Zend/Controller/Action.php';

class Maell_CacheController extends Zend_Controller_Action {

	
	protected $_cache;
	
	
	public function init()
	{
		$this->_cache = \Maell::cacheGetAdapter();
	}
	
	
	public function indexAction()
	{
		//var_dump($cache->getFillingPercentage());
		$html = '<table border="1"><tr><th>Id</th><th>Type</th><th>Date</th><th>TTL</th><th>Expire</th><th>&nbsp;</th></tr>';
		foreach ($this->_cache->getIds() as $id) {
			$metas = $this->_cache->getMetadatas($id);
			$html .= sprintf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s | %s</td></tr>'
							, $id
							, implode(' ', $metas['tags'])
							, date('d/m/Y H:i:s', $metas['mtime'])
							, $metas['expire'] - $metas['mtime']
							, date('d/m/Y H:i:s', $metas['expire'])
							, '<a href="/maell/cache/view/id/' . rawurlencode($id) . '">View</a>'
							, '<a href="/maell/cache/delete/id/' . rawurlencode($id) . '">Delete</a>'
					);
		}
		$html .= '</table>';
		
		echo $html;
	}
	
	
	public function viewAction()
	{
		if (($cached = \Maell::cacheGet($this->_getParam('id'))) !== false) {
			echo '<pre>' . print_r($cached,true) . '<pre>';
		} else {
			throw new \Exception("Unable to find a cached object with this id: " . $this->_getParam('id'));
		}
		exit();
	}
	
	
	public function deleteAction()
	{
		$this->_cache->remove($this->_getParam('id'));
		$this->_redirect('/maell/cache');
		exit();
	}
	
	
	public function cleanAction()
	{
		$this->_cache->clean();
	}
}
