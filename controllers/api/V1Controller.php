<?php

/**
 * DefaultController
 * 
 * @author
 * @version 
 */

use Maell as Maell;
use Maell\ObjectModel;
use Maell\Core\Api;
use Maell\ObjectModel\Collection;
use Maell\Frontend\Api\ApiMethod;
use Maell\ObjectModel\Property\ArrayProperty;
use Maell\ObjectModel\DataObject;
use Maell\ObjectModel\Property\ObjectProperty;

require_once 'Zend/Controller/Action.php';

class Api_V1Controller extends Zend_Controller_Action {

	
	/**
	 * Which format to use to return set : raw or maell-compatible
	 * @var string
	 */
	
	const MODE_RAW = 'raw';
	
	const MODE_MAELL = 'maell';
	
	
	/**
	 * Which type of set to return (default or extended)
	 * @var integer
	 */
	
	const RESULTS_DEF = 0;
	
	const RESULTS_EXT = 1;
	
	
	protected $_status = 'OK';
	
	
	protected $_data = array();
	
	
	protected $_context = array();
	
	
	protected $_method;

	
	/**
	 * Api container
	 * @var Maell\Frontend\Api\RestfulApi
	 */
	protected $_api;
	
	
	protected $_params = array();
	
	
	
	/**
	 * Get the api configuration and check wether it is active and current
	 */
	public function init()
	{	
		//die(bin2hex(openssl_random_pseudo_bytes(24)));
		$this->_context['ts'] = time();
		$this->_context['rm'] = $_SERVER['REQUEST_METHOD'];
		if (isset($_SERVER['CONTENT_TYPE'])) {
			$this->_context['ct'] = $_SERVER['CONTENT_TYPE'];
		}
		
		// Load configuration JIT
		Api::init();
		
		// OPTIONS request
		if ($this->_context['rm'] == Zend_Http_Client::OPTIONS) {
			$this->_status = 'OK';
			$this->postDispatch();
		}
		
		$api = str_replace('Action', '', $this->_getParam('action'));
		$this->_context['api'] = $api;
		
		if (false !== ($this->_api = Api::getApi($api))) {
 			if ($this->_api->getEnabled() === false) {
				// Disabled API
				$this->_status = 'ERR';
				$this->_context['msg'] = "This api is currently disabled";
			} else if ($this->_api->getEndPoint() != $this->_getParam('controller')) {
				// Wrong API version
				$this->_status = 'ERR';
				$this->_context['msg'] = "The requested method doesn't support this API endpoint";
			}
		} else {
			// No such method
			$this->_status = 'ERR';
			$this->_context['msg'] = "The requested method doesn't exist";			
		}
	}
	
	
	/**
	 * Magic method called to match the HTTP method used by the client
	 * @param string $methodName
	 * @param array $args
	 * @return boolean
	 */
	public function __call($methodName, $args)
	{
		if ($this->_status != 'OK') {
			return false;
		}
		
		if ($_SERVER['QUERY_STRING'] == 'info') {
			$this->_data['api'] = $this->_api->getLabel();
			$this->_data['desc'] = $this->_api->getDescription();
			return true;
		}
		
		// Test for HTTP Method
		switch ($this->_context['rm']) {
			
			case Zend_Http_Client::GET:
				if (false == ($method = $this->_api->getMethods()->getMemberFromUri('read'))) {
					$this->_status = 'ERR';
					$this->_context['msg'] = "The HTTP GET method is not allowed";
				} else {
					$this->_method = 'read';
				}
				break;
				
			case Zend_Http_Client::POST:
				if (false == ($method = $this->_api->getMethods()->getMemberFromUri('create'))) {
					$this->_status = 'ERR';
					$this->_context['msg'] = "The HTTP POST method is not allowed";
				} else {
					$this->_method = 'create';
				}
				break;
				
			case Zend_Http_Client::PUT:
				if (false == ($method = $this->_api->getMethods()->getMemberFromUri('update'))) {
					$this->_status = 'ERR';
					$this->_context['msg'] = "The HTTP PUT method is not allowed";
				} else {
					$this->_method = 'update';
				}
				break;
				
			case Zend_Http_Client::DELETE:
				if (false == ($method = $this->_api->getMethods()->getMemberFromUri('delete'))) {
					$this->_status = 'ERR';
					$this->_context['msg'] = "The HTTP DELETE method is not allowed";
				} else {
					$this->_method = 'delete';
				}
				break;
				
			default:
				$this->_status = 'ERR';
				$this->_context['msg'] = "The HTTP request method is not supported";
				break;
		}
		
		if (! $this->_method) {
			return false;
		}
		
		if ($this->_method == 'read') {
			$this->_params = $_GET;
		} else {
			if (strstr($this->_context['ct'],'application/json')) {
				$this->_params = Zend_Json::decode(file_get_contents("php://input"));
			} else {
				 $this->_params = $_POST;
			}
		}
		
		/**
		 * If authentication is required, check token presence and validity
		 */
		if ($method->getAuth()) {
			$authParam  = $method->getAuth('parameter');
			$authMethod = $method->getAuth('method');
			// check token presence
			if (! isset($_GET[$authParam]) || empty($_GET[$authParam])) {
				$this->_status = 'ERR';
				$this->_context['msg'] = "Missing authentication token";
				return false;
			}
				
			// @todo verify token
			if (! is_null($authMethod)) {
					
			}
		}
		
		call_user_func_array(array($this, '_' . $this->_method), array($method));
	}	
		

	protected function _create(ApiMethod $method)
	{
		$obj = new $this->_api->object;
		
		// Test for parameters
		foreach ($method->getApiParameters()->getMembers() as $parameter) {
			$key = $parameter->getIdentifier();
			if ($parameter->getParameter('constraint.mandatory') && (!isset($this->_params[$key]) || empty($this->_params[$key]))) {
				$this->_status = 'ERR';
				$this->_context['msg'] = sprintf("Mandatory parameter '%s' is missing or empty", $key);
				return false;
			}
			if (isset($this->_params[$key])) {
				$value = $this->_params[$key];
			} else if ($parameter->getDefaultValue()) {
				$value = $parameter->getDefaultValue();
			} else {
				$value = null;
			}
			if (! is_null($value)) {
				$obj->$key = $value;
			}
		}
		
		if ($obj->save()) {
			$this->_data['id'] = $obj->getIdentifier();
			return true;
		} else {
			return false;
		}
	}
	
	
	protected function _read(ApiMethod $method)
	{
		$this->_params['mode'] = isset($this->_params['mode']) ? $this->_params['mode'] : self::MODE_RAW;
		$this->_params['offset'] = isset($this->_params['offset']) ? (int) $this->_params['offset'] : 0;
		$this->_params['batch'] = isset($this->_params['batch']) ? (int) $this->_params['batch'] : 30;
		$this->_params['ext'] = isset($this->_params['ext']) ? (int) $this->_params['ext'] : 0;
		
		$collection = new Collection($this->_api->getObject());
		
		$usedParams = 0;
		
		// Test for parameters
		foreach ($method->getApiParameters()->getMembers() as $parameter) {
			$key = $parameter->getIdentifier();
			if ($parameter->getParameter('constraint.mandatory') && (!isset($this->_params[$key]) || empty($this->_params[$key]))) {
				$this->_status = 'ERR';
				$this->_context['msg'] = sprintf("Mandatory parameter '%s' is missing or empty", $key);
				return false;
			}
			if (isset($this->_params[$key])) {
				$value = trim($this->_params[$key]);
			} else if ($parameter->getDefaultValue()) {
				$value = $parameter->getDefaultValue();
			} else {
				$value = null;
			}
			if (! is_null($value) && $value != "") {
				$condition = $collection->having($parameter->getTarget() ? $parameter->getTarget() : $key);
				if ($parameter->getSearchmode()) {
					$condition->{$parameter->getSearchmode()}($value);
				} else {					
					$condition->equals($value);
				}
				$usedParams++;
			}
		}
		
		// Test for parameter usage (at least one is required)
		if ($usedParams == 0) {
			$this->_status = 'ERR';
			$this->_context['msg'] = sprintf("You must provide at least one parameter value");
			return false;			
		}
		
		if ($method->getSortings()) {
			foreach ($method->getSortings() as $prop => $order) {
				$collection->setSorting(array($prop, $order));
			}
		}
		
		$collection->setBoundaryBatch($this->_params['batch']);
		$collection->setBoundaryOffset($this->_params['offset'] > 0 ? $this->_params['offset'] : 0);
		
		$this->_context['max'] = $collection->count();
		$this->_context['offset'] = $collection->getBoundaryOffset();
		$this->_context['batch'] = $collection->getBoundaryBatch();
		
		if ($this->_context['max'] > 0) {
			$properties = array_keys($method->getProperties());
			if ($this->_params['ext'] == self::RESULTS_EXT && $method->getExtProperties()) {
				$extproperties = array_keys($method->getExtProperties());
				$properties = array_merge($properties, $extproperties);
			}
			$this->_data = [];
			foreach ($collection->getMembers(ObjectModel::DATA) as $member) {
				if ($this->_params['mode'] == self::MODE_MAELL) {
					$this->_data[] = $member->reduce(array('params' => array(), 'properties' => $properties));
				} else {
					$set = [];
					foreach ($properties as $key) {
						if (strstr($key,'.') !== false) {
							$parts = explode('.', $key);
							$property = $member->getProperty($parts[0]);
							
							// recursion on array
							if ($property instanceof ArrayProperty) {
								$value = $property->getValue();
								for ($i = 1 ; $i < count($parts) ; $i++) {
									$value = $value[$parts[$i]];
								}
								$set[$parts[0]] = $value;
								continue;
							} else if ($property instanceof ObjectProperty) {
								$value = $property->getValue(ObjectModel::DATA);
								$set[$parts[0]] = $value->getProperty($parts[1])->getValue(isset($parts[2]) ? $parts[2] : null);
								continue;							
							}
						}
						$set[$key] = $member->$key;
					}
					$this->_data[$this->_getDataKeyFromMember($member, $method)] = $set;
				}
			}
		}
	}
	
	
	/**
	 * Process and return the correct value to be used as unique key for the current member data
	 * @param DataObject $member
	 * @param ApiMethod $method
	 * @return string
	 */
	protected function _getDataKeyFromMember(DataObject $member, ApiMethod $method)
	{
		if (false !== ($dk = $method->getDataKey())) {
			if (strstr($dk, '.') !== false) {
				$parts = explode('.', $dk);
				return $member->getProperty($parts[0])->getValue($parts[1]);
			} else {
				return (string) $member->{$dk};
			}
		} else {
			return $member->getIdentifier();
		}
	}
	
	
	protected function _update($method)
	{
		
	}
	
	
	protected function _delete($method)
	{
		
	}
	
	
	public function postDispatch()
	{
		exit($this->_setResponse($this->_status, $this->_data, $this->_context));
	}
	
	
	protected function _setResponse($status = 'OK', $data, $context = null, $format = 'json')
	{
		$response = array('status' => $status, 'data' => $data, 'context' => $context);
		
 		switch ($format) {
 			
 			case 'json':
 			default:
 				header('Content-Type:application/json');
	 			header('Access-Control-Allow-Origin: *');
 				header('Access-Control-Allow-Headers: Overwrite, Destination, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control');
 				header('Access-Control-Allow-Methods: PROPFIND, PROPPATCH, COPY, MOVE, DELETE, MKCOL, LOCK, UNLOCK, PUT, GETLIB, VERSION-CONTROL, CHECKIN, CHECKOUT, UNCHECKOUT, REPORT, UPDATE, CANCELUPLOAD, HEAD, OPTIONS, GET, POST');
 				header('Access-Control-Allow-Credentials: true');
 				if (Maell::getEnvData('cache_datasets') !== false) {
	 				header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
 					header_remove('Cache-Control');
 					header_remove('Pragma');
 				}
 				return \Zend_Json::encode($response);
 				break;
 		}	
	}
}
