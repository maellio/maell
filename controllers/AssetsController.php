<?php

/**
 * Assets delivery controller
 * 
 * @author
 * @version 
 */


require_once 'Zend/Controller/Action.php';

/**
 * Assets elements loader
 *
 */
class Maell_AssetsController extends \Zend_Controller_Action {

	
	/**
	 * Array of acceptable mime types
	 * @var array
	 */
	protected $_mimetypes = array(
	                                'html'  => 'text/html',
									'js'	=> 'application/javascript', 
									'css'	=> 'text/css', 
									'png'	=> 'image/png', 
									'gif'	=> 'image/gif',
									'jpg'	=> 'image/jpeg',
									'ttf'   => 'application/octet-stream',
	                                'svg'	=> 'image/svg'
								 );
	
	
	public function __call($methodName, $args)
	{
		if ($this->_getParam('action')) {
		    Maell::log("Requested asset " . implode('/',$this->_getAllParams()));
 			$params = array_slice($this->_getAllParams(),3,1);
			if (count($params) == 0) {
				die("incorrect url format, should be /maell/assets/<ns>/<type>/file.<ext>");
			}
			
			$basepath = \Maell::$maellPath;
			if (substr($basepath, -1) == DIRECTORY_SEPARATOR) {
			    $basepath = substr($basepath,0, strlen($basepath)-1);
			}
			
			$segments = array(
								$basepath, 
								'assets',
								implode(array_keys($params)),
							 );
			
			$filepath = implode(DIRECTORY_SEPARATOR, $segments) . DIRECTORY_SEPARATOR;
			$filename = str_replace(':', '/', current($params));
			$this->_sendResponse($filepath . $filename);
		}
	}


	protected function _sendResponse($filepath)
	{
	    if (! array_key_exists(substr($filepath, strrpos($filepath,'.')+1), $this->_mimetypes)) {
	        $this->getResponse()->setHttpResponseCode(500)->sendResponse();
	        exit();
	    }
	    
	    Maell::log("Sending file " . $filepath);
	     
	    // flush log
	    // @see http://framework.zend.com/manual/1.9/en/zend.log.writers.html#zend.log.writers.firebug
	    $response = new Zend_Controller_Response_Http();
	    $channel = Zend_Wildfire_Channel_HttpHeaders::getInstance();
	    $channel->setRequest(new Zend_Controller_Request_Http());
	    $channel->setResponse($response);
	    $channel->flush();
	    $response->sendHeaders();
	    
	    if (file_exists($filepath)) {
	        // @todo add expires header in production mode ?
	        $file = file_get_contents($filepath);
	        $this->getResponse()->setHeader('Content-type', $this->_mimetypes[substr($filepath, strrpos($filepath,'.')+1)]);
	        $this->getResponse()->setBody($file);
	        $this->getResponse()->sendResponse();
	    } else {
	        $this->getResponse()->setHttpResponseCode(404)->sendResponse();
	    }
	    exit();
	}
}
