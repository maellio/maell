if (! window.maell) {
	window.maell = [];
}

if (! window.maell.locale) {
	
	window.maell.locale = {lang:'en'};

	/**
	 * Function that returns the locale version of the given key for the given lang or default
	 * ex: maell.locale.get('yes'), maell.locale.get('no','fr'), maell.locale.get('wd:sunday')
	 * 
	 * @param key
	 * @param lang
	 * @returns string
	 */
	window.maell.locale.get = function(key, params) {
		params = params || {};
		lang = params.lang || maell.locale.lang;
		obj = params.obj || maell.locale[lang];
		vars = params.vars || null;

		if (key.indexOf(':') != -1) {
			var parts = key.split(':');
			params.obj = maell.locale[lang][parts[0]];
			return maell.locale.get(parts[1], params);
		}
		
		if (obj && obj[key]) {
			if (vars==null) {
				return obj[key];
			} else {
				var str = obj[key];
				for (i in vars) {
					str = str.replace('%'+i, vars[i]);
				}
				return str;
			}
		} else if (lang != 'en') {
			params.lang = 'en';
			return maell.locale.get(key, params);
		}
		return key;
	};
	
	//short form: maell.lget('yes'); 
	window.maell.lget = maell.locale.get;
	window.maell.lget.fr = function (k, p) {
		p = p || {};
		p.lang = 'fr';
		return maell.lget(k, p);
	};
	
	// LOCALES

	
	/**
	 * English messages
	 */
	window.maell.locale.en = {
			
		yes:'yes',
		no:'no',
		cancel:'Cancel',
		back:'Back',
		save:'Save',
		newval:'New Value',
		confirm:{
			button:'Confirm',
			title:'Confirmation Request',
			message:"Please confirm the demanded action",
			remove:"Please confirm the removing of chosen element",
			forbidden:"Forbidden Action"
		},
		wd:{ // weekdays
			sunday:'Sunday',
			monday:'Monday',
			tuesday:'Tuesday',
			wednesday:'Wednesday',
			thursday:'Thursday',
			friday:'Friday',
			saturday:'Saturday'
		},
		ac:{ // autocompleter
			noresult:'No result to display',
			oneresult:'One result only',
			manyresults:'%0 results',
			moreresults:'%0 results displayed in a set of %1',
			extend:'More results'
		},
		err:{
			lbl_srv:'Server Error',
			bkd:'Server error, please refresh the page',
			remove:"An error occured while removing element",
			last:"An array requires at least one value",
			empty:"Please type in a value before clicking",
			selector:"Please select a valid value in the selector before clicking"
		},
		form:{
			saveok:"Successfully saved",
			fielderr:"Required value for",
			emailerr:"is not a valid email address",
			savenew:"Save & New"
		}
	};
	
	
	/**
	 * French messages
	 */
	window.maell.locale.fr = {
			
		yes:'oui',
		no:'non',
		cancel:'Annuler',
		back:'Retour',
		save:'Sauver',
		newval:'Nouvelle Valeur',
		confirm:{
			button:'Confirmer',
			title:'Demande de confirmation',
			message:"Veuillez confirmer l'action demandée",
			remove:"Veuillez confirmer la suppression de l'élément sélectionné",
			forbidden:"Action interdite"
		},
		wd:{
			sunday:'Dimanche',
			monday:'Lundi',
			tuesday:'Mardi',
			wednesday:'Mercredi',
			thursday:'Jeudi',
			friday:'Vendredi',
			saturday:'Samedi'
		},
		ac:{
			noresult:'Aucun résultat à afficher',
			oneresult:'Un seul résultat',
			manyresults:'%0 résultats',
			moreresults:'%0 résultats affichés sur un total de %1',
			extend:'Plus de résultats'
		},
		err:{
			lbl_srv:'Une erreur a été signalée par le serveur',
			bkd:'Erreur serveur, veuillez rafraîchir la page',
			remove:"Erreur lors de la suppression",
			last:"Vous ne pouvez pas supprimer la seule valeur définie",
			empty:"Veuillez saisir une valeur avant de valider",
			selector:"Veuillez sélectionner une valeur valide dans le sélecteur avant d'ajouter"
		},
		form:{
			saveok:"Sauvegarde effectuée",
			fielderr:"Valeur requise pour le champ",
			emailerr:"n'est pas une adresse mél valide",
			savenew:"Sauvez & Nouveau"
		}
	};
}