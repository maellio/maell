<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core;
use Maell\ObjectModel\BaseObject;
use Maell\Backend;

/**
 * Simple session handler
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Session extends BaseObject {

	
	public function __construct($val = null, array $params = null)
	{
		parent::__construct($val,$params);
		
		$this->start = $this->latest = date('U');
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->browser = $_SERVER['HTTP_USER_AGENT'];
	}
	
	
	public function __wakeup()
	{
		if (! $this->isExpired()) {
			$this->setLatest(time('U'));
			return $this->save();
		}
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Maell\ObjectModel\BaseObject::save()
	 */
	public function save(Backend\Adapter\AbstractAdapter $backend = null)
	{
		if (is_null($backend)) {
			$this->setKey(\Maell::cacheSet($this, $this->getKey(), true, array('tags' => 'session')));
			return true;
		} else {
			return parent::save($backend);
		}
	}
	
	
	public function isExpired()
	{
		return (time('U') - $this->getLatest()) > $this->getParameter('latency');
	}
	
	
	public function getStart($hr = false)
	{
		return $hr ? date('Y-m-d H:i:s', $this->_start) : $this->_start;
	}
	
	
	protected function toDate($timestamp)
	{
		
	}
}
