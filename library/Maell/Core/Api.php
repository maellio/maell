<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell;
use	Maell\Config;
use Maell\Frontend\Api as ApiClasses;

/**
 * Class managing modules
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Api {

	
    /**
     * Describe a RESTFUL api
     * @var string
     */
	const RESTFUL = 'restful';

	
	/**
	 * Path to modules base directory
	 * @var string
	 */
	static protected $_path;
	
	/**
	 * Array of detected modules
	 * @var array
	 */
	static protected $_modules;
	
	
	/**
	 * Config store
	 * @var array
	 */
	static protected $_config;
	
	
	/**
	 * Detect all modules directories and try to get api config from them
	 */
	public static function init()
	{
		if (\Maell::getEnvData('cache_configs') !== false) {
			$ckey = 'configs_api';
			if (($cached = \Maell::cacheGet($ckey)) !== false) {
				self::$_config = $cached;
				return;
			}
		}
		
		// load application api configuration files
		$config = Config\Loader::loadConfig('apis.xml', Config::REALM_CONFIGS);
		self::$_config = $config['apis'];
		if (isset($ckey)) {
			\Maell::cacheSet($config['apis'], $ckey, true, array('tags' => array('config','apis')));
		}
	}
	
	
	/**
	 * Return the current config
	 * @return array
	 */
	public static function getConfig()
	{
		return self::$_config;
	}
	
	
	/**
	 * Return the definition of the API matching the given id or false
	 * @param string $id
	 * @return \Maell\Frontend\Api\RestfulApi|boolean
	 */
	public static function getApi($id)
	{
		if (isset(self::$_config[$id])) {
			$api = self::factory(self::$_config[$id], $id);
			return $api;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Instanciate an API from a config array
	 * @param array $config
	 * @param string $id
	 * @return \Maell\Frontend\Api\RestfulApi
	 */
	public static function factory(array $config, $id)
	{
		switch ($config['type']) {
			
			case self::RESTFUL:
				$api = new ApiClasses\RestfulApi();
				$api->getDataObject()->populate($config);
				$api->setUri($id);
				break;
		}
		return $api;
	}
	
	
	/**
	 * Extract ACL definitions from a module configuration array
	 * @param string $key
	 * @param array $array
	 * @return array
	 */
	protected static function _getApi($key, $array) 
	{
		$acl = array();
		
		foreach ($array as $path => $data) {
			if (isset($data['items'])) {
				$acl += self::_getAcl($key, $data['items']);
			
			} else {
				if (! isset($data['acl'])) $data['acl'] = array('all' => self::GRANTED);
				$acl[$key . '/' . $path] = $data['acl'];
			}
		}
		return $acl;
	}
}
