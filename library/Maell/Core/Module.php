<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell as Maell;
use Maell\Config;

/**
 * Class managing modules
 *
 * @category   maell
 * @package    Maell_Modules
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Module {


	/**
	 * Path to modules base directory
	 * @var string
	 */
	static protected $_path;
	
	/**
	 * Array of detected modules
	 * @var array
	 */
	static protected $_modules;
	
	
	static protected $_config;
	
	
	/**
	 * Detect all modules directories and try to get config for them
	 * @param string $path
	 */
	public static function init($path)
	{
		self::$_path = $path . 'application/modules' . DIRECTORY_SEPARATOR;
		if (! is_dir(self::$_path)) return false;
		
		// get config from cache
		if (\Maell::getEnvData('cache_configs') == "bogus") {
			$ckey = 'configs_modules';
			if (($cached = \Maell::cacheGet($ckey)) !== false) {
				self::$_config = $cached;
			}
		}
		
		if (true) { //! self::$_config) {
		
			self::$_config = array();
			self::$_modules = array('enabled' => array(), 'disabled' => array());
		
			// detect modules in application/modules
			self::_addPaths(self::$_path);

			// detect modules in maell/modules
			self::_addPaths(\Maell::$basePath . 'vendor/maellio/maell/modules/');
			
			// load all detected modules configuration file
			$config = Config\Loader::loadConfig('module.xml', Config::REALM_MODULES);
			
			// remove useless "modules" key
			foreach ($config as $key => $val) {
				self::$_config[$key] = $val['modules'];
			}
			
			// cache config if cache is enabled
			if (isset($ckey)) {
				\Maell::cacheSet(self::$_config, $ckey);
			}
		}
		
		unset($ckey);
		
		
		// build menu
		if (\Maell::getEnvData('cache_configs') == "bogus") {
			$ckey = 'configs_menu_main';
			if (($cached = \Maell::cacheGet($ckey)) !== false) {
				$menu = $cached;
			}	
		}
		
		if (true) { //! isset($menu)) {

			$menu = new Layout\Menu('main');
			$menu->setLabel('Main Menu');
		
			foreach (self::$_config as $prefix => $modules) {
			
				foreach ($modules as $key => $module) {
					
					if ($module['enabled'] != 'true') continue;
				
					$path = \Maell::$basePath;
					if (isset($module['path'])) {
						$path .= $module['path'];
					} else {
						$path .= 'application/modules';
					}
					$path .= DIRECTORY_SEPARATOR . $prefix . DIRECTORY_SEPARATOR . $key;
					self::bind($module, $path);
				
					// if module has parameters, declare them
					if (isset($module['parameters']) && is_array(isset($module['parameters']))) {
						foreach ($module['parameters'] as $paramKey => $array) {
							$parameter = Parameter::factory($array);
							\Maell::addModuleParameter($key, $paramKey, $parameter);
						}
					}
							
					
					// if module has controllers, declare them to front controller
					// then add them to main menu
					if (isset($module['controller']) || isset($module['controllers_extends'])) {
						Config::addPath($path . '/controllers/', Config::REALM_CONTROLLERS, null, $module['controller']['base']);
					}
				
					if (isset($module['controller'])) {				
						$menu->addItem($module['controller']['base'], $module['controller']);
					}
				
					// if module extends existing controllers, declare them for inclusion at the end of the process
					if (isset($module['controllers_extends']) && ! empty($module['controllers_extends'])) {
						$menu->registerExtends($module['controller']['base'], $module['controllers_extends']);
					}
				}
			}
		
			// process extends declaration when menu is complete
			$menu->proceedExtends();
			
			if (isset($ckey)) {
				\Maell::cacheSet($menu, $ckey);
			}
		}
		
		Layout::addMenu('main', $menu);
	}
	
	
	protected static function _addPaths($basedir)
	{
		foreach (scandir($basedir) as $vendor) {
			if (is_dir($basedir . $vendor) && substr($vendor, 0, 1) != '.') {
				foreach (scandir($basedir . $vendor) as $entry) {
					$fpath = $basedir . $vendor . DIRECTORY_SEPARATOR . $entry . DIRECTORY_SEPARATOR;
					if (is_dir($fpath) && substr($entry, 0, 1) != '.') {
						if (is_dir($fpath . 'configs')) {
							// register path with $vendor as prefix
							Config::addPath($fpath . 'configs', Config::REALM_MODULES, null, $vendor);
						}
					}
				}
			}
		}
	}
	
	
	public static function bind(array $config, $path, Layout\Menu $menu = null)
	{
		$store = ($config['enabled'] == true) ? 'enabled' : 'disabled';
		
		if ($store == 'disabled') {
			return;
		}
		
		Config::addPath($path . '/configs/', Config::REALM_CONFIGS);
		
		// if modules has model, declare path to the autoloader
		if (is_dir($path . '/models') && isset($config['namespace'])) {
			Maell::getAdapter('autoload')->add($config['namespace'], $path . '/models/');
			Config::addPath($path . '/models', Config::REALM_OBJECTS, null, $config['namespace']);
		}
		
		// Register views directory if it exists
		if (is_dir($path . '/views')) {
			Config::addPath($path . '/views', Config::REALM_TEMPLATES, Config::POSITION_TOP);
		}
	}
	
	
	public static function getConfig()
	{
		return self::$_config;
	}
}
