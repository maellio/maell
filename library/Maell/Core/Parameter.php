<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Exception;

/**
 * Class providing objects and modules parameters wrapper ensuring basic logic control.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Parameter implements ClientSideInterface {

	
	/**
	 * Allowed values for parameter
	 *
	 * @var array
	 */
	protected $_values = array();
	
	
	/**
	 * Parameter value
	 *
	 * @var mixed
	 */
	protected $_value;
	

	/**
	 * Is parameter value protected ?
	 *
	 * @var boolean
	 */
	protected $_protected = false;
	
	
	/**
	 * Class instance expected from value
	 * @var string
	 */
	protected $_valuesInstanceof;
	
	
	/**
	 * Parameter constructor
	 *
	 * @param string $type Parameter type
	 * @param mixed $value Parameter value
	 * @param boolean $protected Parameter protection flag
	 * @param array $values Acceptable values for parameter
	 * @param string $viof Class instance of value-s
	 */
	public function __construct($type = \Maell\Parameter::ANY, $value = null, $protected = false, array $values = null, $viof = null)
	{
	    $this->_valuesInstanceof = $viof;
		$this->_setType($type);
		$this->_setProtected($protected);
		if (is_array($values)) {
			$this->_setValues($values);
		}
		$this->setValue($value);
	}
	
	
	protected function _setType($type)
	{
		if (in_array($type, array(\Maell\Parameter::ANY, \Maell\Parameter::BOOLEAN, \Maell\Parameter::INTEGER, \Maell\Parameter::FLOAT, \Maell\Parameter::STRING, \Maell\Parameter::MULTIPLE, \Maell\Parameter::OBJECT))) {
			$this->_type = $type;
		} else {
			throw new Exception("'$type' is not an acceptable type");
		}		
	}
	
	
	protected function _setProtected($bool)
	{
		$this->_protected = (bool) $bool;
	}
	
	
	protected function _setValues($values)
	{
		$this->_values = $values;
	}
	
	
	/**
	 * Set parameter value
	 * @param mixed $value
	 * @param string $key
	 * @throws \Maell\Exception
	 * @return \Maell\Core\Parameter
	 */
	protected function _setValue($value, $key = null)
	{
		if (is_object($value) && count($this->_values) != 0 && !in_array(get_class($value), $this->_values)) {
			throw new Exception(array("OBJECT_NOT_INSTANCEOF", array((string) $value, implode(',', $this->_values))));
		} else if (count($this->_values) > 0 && !in_array($value, $this->_values)) {
			throw new Exception(array("VALUE_NOT_IN_ENUMERATION", array($value, implode(',', $this->_values))));
		}
		
		if ($key !== null) {
    		if ($this->_valuesInstanceof && isset($this->_value[$key])) {
    		    $this->_value[$key]->setValue($value);
    		} else {
    		    $this->_value[$key] = $value;
    		}
		} else {
    		$this->_value = $value;
		}	
		return $this;
	}
	

	/**
	 * Do some simple checks before setting parameter value
	 * @param mixed $value
	 * @throws \Maell\Exception
	 */
	public function setValue($value = null, $key = null)
	{
		if ($this->_protected && $this->_value != null && $key == null) {
			throw new Exception(["VALUE_PROTECTED_FROM_CHANGE",[$value, $this->_value]]);
		}
		
		if (! is_null($value)) {
			switch ($this->_type) {
				case \Maell\Parameter::BOOLEAN:
					if (! is_null($value) && ! is_bool($value)) {
					    throw new Exception(["VALUE_NO_BOOLEAN", [$value]]);
					}
					break;
				
				case \Maell\Parameter::INTEGER:
					if (! is_integer($value) || ! is_numeric($value)) {
					    throw new Exception(["VALUE_NO_INTEGER",[$value]]);
					}
					break;
				
				case \Maell\Parameter::FLOAT:
					if (! is_float($value)) {
					    throw new Exception(["VALUE_NO_FLOAT",[$value]]);
					}
					break;
				
				case \Maell\Parameter::OBJECT:
					if (! is_object($value)) {
					    throw new Exception(array("VALUE_MUST_BE_INSTANCEOF", [$value]));
					}
					break;
				
				case \Maell\Parameter::STRING:
						if (! is_string($value)) {
						    throw new Exception(array("VALUE_NO_STRING", [$value]));
						}
					break;
				
				case \Maell\Parameter::MULTIPLE:
					if (! is_null($value) && (! is_array($value) && is_null($key))) {
					    throw new Exception(array("VALUE_NO_ARRAY", [$value]));
					}
					break;
			}
		}
		return $this->_setValue($value, $key);
	}
	
	
	/**
	 * Return the current parameter value or the value of a given member 
	 * if key is provided and parameter type is array
	 * @param string $key
	 * @return mixed
	 */
	public function getValue($key = null)
	{
		if ($key && $this->_type == \Maell\Parameter::MULTIPLE) {
			if (isset($this->_value[$key])) {
			    if ($this->_value[$key] instanceof self) {
			        // array is a collection of parameters
			        return $this->_value[$key]->getValue();
			    } else {
			        return strlen($this->_value[$key]) > 0 ? $this->_value[$key] : true;
			    }
			} else {
			    return false;
			}
		} else {
			return $this->_value;
		}
	}
	
	
	public function getType()
	{
		return $this->_type;
	}
	

	public function getValues()
	{
	    return $this->_values;
	}
	

	public function getValuesInstance()
	{
	    return $this->_valuesInstanceof;
	}
	
	
	public function isProtected()
	{
		return $this->_protected;
	}
	
	
	public function toArray()
	{
	    $_ = ['type' => $this->_type, 'protected' => $this->_protected];
	    if (count($this->_values) > 0) {
	        $_['values'] = $this->_values;
	    }
	    if (is_array($this->_value)) {
	        $__ = [];
	        foreach ($this->_value as $k => $v) {
	            $__[$k] = $v instanceof self ?$v->toArray() : $v;
	        }
	        $_['value'] = $__;
	    } else {
	        $_['value'] = $this->_value;
	    }
	    return $_;
	}
	
	
	public function reduce(array $params = array(), $cache = true)
	{
		return $this->_value; //
		return array('id' => $this->_id, 'type' => $this->_type, 'value' => $this->_value);
	}
}
