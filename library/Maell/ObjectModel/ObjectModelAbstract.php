<?php

namespace Maell\ObjectModel;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core;
use Maell\Core\Parameter;
use Maell\ObjectModel\Rule\AbstractRule;

/**
 * Class providing basic functions needed to handle environment building.
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
abstract class ObjectModelAbstract implements Core\ClientSideInterface {

	
	/**
	 * Object unique identifier
	 *
	 * @var string
	 */
	protected $_id;
	
	
	/**
	 * Object parameters
	 * a collection of Maell\Parameter instances
	 *
	 * @var array
	 */
	protected $_params = array();
	
	
	/**
	 * Array of rules to apply on defined trigger
	 * @var array
	 */
	protected $_rules;
	
	
	/**
	 * Class constructor, defines object id, parameters and their values
	 * 
	 * @param string $id
	 * @param array $params
	 */
	public function __construct($id = null, array $params = null)
	{
		if (! is_null($id)) $this->setId($id);

		$this->_setParameterObjects();
		
		if (is_array($params)) {
			$this->_setParameters($params);
		}
	}
	
	
	/**
	 * Sets object id
	 * 
	 * @param string $id
	 * @return Maell\ObjectModel\ObjectModelAbstract
	 */
	public function setId($id)
	{
		if (! is_null($this->_id)) {
		//	throw new Exception(array("OBJECT_CANNOT_CHANGE_VALUE", '$id'));
		}
		
		$this->_id = $id;
		return $this;
	}
	
	
	public function getId() 
	{ 
		return $this->_id;
	}		
	
	
	/**
	 * PARAMETERS HANDLING METHODS SECTION
	 */
	
	
	/**
	 * Return the current value of the parameter object matching the given key or key pattern
	 *
	 * @param string $key		parameter key. The simplest form is a string. 
	 * 							The dot is used to directly get an array-type property value (ex: propname.arraykey)
	 * @param boolean $strict	Strict mode. Set to true to throw an exception if parameter does not exist
	 * @return mixed parameter value
	 */
	final public function getParameter($key, $strict = false)
	{
		$arraykey = null;
		if (strstr($key, '.') !== false) list($key, $arraykey) = explode('.', $key);
		
		if (isset($this->_params[$key]) && $this->_params[$key] instanceof \Maell\Core\Parameter) {
			return $this->_params[$key]->getValue($arraykey);
		} else {
			if ($strict === false) {
				return null;
			} else {
				throw new Exception(array("NO_SUCH_PARAMETER", $key));
			}
		}
	}
	
/* 	/**
	 * Set the value of the matching parameter or change it if it is not protected
	 *
	 * @param string $key
	 * @param mixed $value
	 
	public function setParameter($key, $value)
	{
		$arraykey = null;
		if (strstr($key, '.') !== false) {
			list($key, $arraykey) = explode('.', $key);
		}
		
		if (isset($this->_params[$key]) && $this->_params[$key] instanceof \Maell\Core\Parameter) {
			try {
			    
				if ($arraykey) { // parameter stores an array
					$values = (array) $this->_params[$key]->getValue();
					$values[$arraykey] = $value;
					$value = $values;
				}
				$this->_params[$key]->setValue($value);
			} catch (Exception $e) {
				throw new Exception("Unable to define '$key' parameter with value '$value'");
			}
		}
		return $this;
	} */
	
	
	/**
	 * Set the value of the matching parameter or change it if it is not protected
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function setParameter($key, $value)
	{
	    $arraykey = null;
	    if (strstr($key, '.') !== false) {
	        list($key, $arraykey) = explode('.', $key);
	    }
	
	    if (isset($this->_params[$key]) && $this->_params[$key] instanceof \Maell\Core\Parameter) {
	        try {
	             
/* 	            if ($arraykey) { // parameter stores an array
	                $values = (array) $this->_params[$key]->getValue();
	                $values[$arraykey] = $value;
	                $value = $values;
	            } */
	            $this->_params[$key]->setValue($value, $arraykey);
	        } catch (Exception $e) {
	            throw new Exception("Unable to define '$key' parameter with value '$value'");
	        }
	    }
	    return $this;
	}
	
	
	public function getParameters()
	{
		return $this->_params;
	}
	
	
	/**
	 * Allow setting values for multiple parameters from an array
	 *
	 * @param array $params
	 */
	final protected function _setParameters(array $params)
	{
	    unset($params['type']);
	     
		foreach ($params as $key => $value) {
		    if (! isset($this->_params[$key])) {
		        // object definition shouldn't include values for parameters that doesn't exist
		        throw new Exception("Parameter '$key' doesn't exist in " . get_class($this));
		    }
		    $parameter = $this->_params[$key];
		    if ($parameter->getType() == \Maell\Parameter::MULTIPLE && is_array($value)) {
		        foreach ($value as $akey => $aval) {
		            if (is_string($aval) && strlen($aval) == "") {
		                $aval = true;
		            }
		            $parameter->setValue($aval, $akey);
		        }
		    } else {
    			$parameter->setValue($value);
		    }
		}
	}
	
	
	/**
	 * Add multiple parameters to the object from array or default config
	 *
	 * @param array $objects	if parameter is null, parameters are acquired from xml configuration files
	 * @param boolean $replace
	 */
	final protected function _setParameterObjects(array $objects = null, $replace = false)
	{
		if (count($objects) == 0) {
			$objects = (array) \Maell\Parameter::getParameters($this);
		}
		
		if ($replace === true) {
			$this->_params = array();
		}
		
		foreach ($objects as $key => $object) {
			if (! $object instanceof Parameter) {
				continue;
			}
			$this->_params[$key] = $object;
		}
	}
	
	
	/**
	 * RULES
	 */
	

	/**
	 * Attach a rule instance and its trigger to the property
	 * @param ObjectModel\Rule\AbstractRule $rule
	 * @param unknown_type $trigger
	 */
	public function attach(AbstractRule $rule, $trigger)
	{
		if (! isset($this->_rules[$trigger]) || ! is_array($this->_rules[$trigger])) {
			$this->_rules[$trigger] = array();
		}
		$this->_rules[$trigger][] = $rule;
		return $this;
	}
	
	
	public function getRules()
	{
		return $this->_rules;
	}


	/**
	 * Execute defined rules for given trigger
	 *
	 * @param string $trigger
	 * @param mixed $params
	 * @return boolean
	 */
	protected function _triggerRules($trigger, $params = null)
	{
		if (! isset($this->_rules[$trigger])) {
			/* return true if no defined rule for trigger */
			return true;
		}
	
		$result = true;
	
		foreach ($this->_rules[$trigger] as $rule) {
			$result = $result && $rule->execute($params);
		}
	
		return $result;
	}
	
	
	public function __clone()
	{
		foreach ($this->_params as $key => $param) {
			$this->_params[$key] = clone $param;
		}
	}
	
	
	public function register()
	{
		$res = \Maell::cacheSet($this);
		if ($res !== false) {
			$this->_id = $res;
		}
		return $this->_id;
	}
	
	
	/**
	 * Reduce parameters to a simple array
	 * @see Maell\Core.ClientSideInterface::reduce()
	 * @return array
	 */
	public function reduce(array $params = array(), $cache = true)
	{
		if (isset($params['params']) && count($params['params']) == 0) return array();
		$parameters = array();
		foreach ($this->_params as $key => $parameter) {
			if (isset($params['params']) && is_array($params['params']) && ! array_key_exists($key, $params['params'])) {
				continue;
			}
			
			if (is_null($parameter->getValue())) continue;
			$parameters[$key] = $parameter->reduce($params, $cache);
		}
		return count($parameters) > 0 ? array('params' => $parameters) : array();
	}
}
