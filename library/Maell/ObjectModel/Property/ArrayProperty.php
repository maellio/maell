<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Locale;

/**
 * Class for an Array Property
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class ArrayProperty extends AbstractProperty {

	
    /**
     * (non-PHPdoc)
     * @see \Maell\ObjectModel\Property\AbstractProperty::setValue()
     */
	public function setValue($value)
	{
		// try to detect serialized values
		if (!is_array($value) && @unserialize($value) !== false) {
			$value = unserialize($value);
		}
		
		if (! is_array($value)) {
			throw new Exception(sprintf("The '%s' property accepts only arrays", $this->_id));
		}
		parent::setValue($value);
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Maell\ObjectModel\Property\AbstractProperty::getValue()
	 */
	public function getValue($key = null)
	{
		if (is_null($key)) {
			return parent::getValue();
		} else {
			if (isset($this->_value[$key])) {
				return $this->_value[$key];
			} else {
				return false;
			}
		}
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Maell\ObjectModel\Property\AbstractProperty::getDisplayValue()
	 */
	public function getDisplayValue()
	{
	    if ($this->getParameter('constraints.multilingual')) {
	        return Locale::get($this->_value);
	    } else {
	        return implode(',', $this->_value);
	    }
	}
}
