<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Class for an Currency Property
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CurrencyProperty extends AbstractProperty {

	
	public function setValue($value)
	{
		if (! empty($value) && ! is_numeric($value) && ! is_float($value) && ! is_integer($value)) {
			throw new Exception(array("VALUE_NOT_INTEGER_OR_FLOAT", $value));
		}
		parent::setValue((float) $value);
	}
	
	
	public function getDisplayValue()
	{
		$precision = $this->getParameter('constraints.precision') ? $this->getParameter('constraints.precision') : 2;
		return self::format($this->_value, $precision, $this->getParameter('currency'));
	}
	
	
	static public function format($value, $precision = 4, $currency = 'fr_FR')
	{
		setlocale(LC_MONETARY, $currency); //fr_FR.UTF-8');
		$format = '%.' . $precision . 'n';
		return money_format($format, $value);
	}
}
