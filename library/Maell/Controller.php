<?php

namespace Maell;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Config
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Config;
use Maell\Core\Module;

/**
 * Class providing basic functions needed to manage Configuration files
 *
 * @category   maell
 * @package    Maell
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

class Controller {
	
	const ZEND1 = "Z1";
	
	const ZEND2 = "Z2";
	
	
	static protected $_instance;
	
	static protected $_routes = array();

	
	static public function init()
	{		
		foreach (Config::getPaths(Config::REALM_CONTROLLERS) as $controller) {
			list($path, $prefix) = explode(Config::PREFIX_SEPARATOR, $controller);
			self::$_routes[$prefix] = $path;
		}
		self::factory();
	}
	
	
	/**
	 * Init controller (if necessary) and dispatch
	 */
	static public function dispatch()
	{
		if (! self::$_instance) {
			self::init();
		}
		
		if (self::$_instance instanceof \Zend_Controller_Front) {
			self::$_instance->dispatch();
		}
	}
	
	
	static public function factory($adapter = self::ZEND1)
	{
		switch ($adapter) {
			
			case self::ZEND1:
			default:
				self::$_instance = \Zend_Controller_Front::getInstance();
				self::$_instance->throwExceptions(true);
				self::$_instance->setParam('noViewRenderer', true);
				self::$_instance->setControllerDirectory(self::$_routes);
				foreach (Module::getConfig() as $vendor => $modules) {
					foreach ($modules as $module) {
						if (isset($module['zendframework'])) {
							foreach ($module['zendframework'] as $stackIndex => $pluginClass) {
								try {
									$plugin = new $pluginClass();
								} catch (\Exception $e) {
									throw new Exception("'$pluginClass' is not a Zend Framework 1 controller plugin");
								}
								self::$_instance->registerPlugin($plugin, $stackIndex);
							}
						}
					}
				}
				break;
		}
	}
}
