<?php

namespace Maell\Backend\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 832 $
 */

use Maell\Backend;
use Maell\ObjectModel;
use Maell\ObjectModel\Property;

/**
 * Interface for backend adapters
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * 
 */


interface AdapterInterface {

	
	public function __construct(Backend\BackendUri $uri);
	
	
	public function setMapper(Backend\Mapper $mapper);
	
	
	public function create(ObjectModel\DataObject $do);
	
	
	public function read(ObjectModel\DataObject $do);
	
	
	public function update(ObjectModel\DataObject $do);
	
	
	public function delete(ObjectModel\DataObject $do);

	
	public function find(ObjectModel\Collection $collection);
	
	
	public function loadBlob(ObjectModel\DataObject $do, Property\AbstractProperty $property);
	
	
	public function transactionStart($key = null);
	
	
	public function transactionCommit();
	
	
	public function transactionExists();
}
