<?php

namespace Maell\View\ListComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View;
use Maell\View\ViewUri;
use Maell\View\Decorator;
use Maell\View\FormComponent\Element\ButtonElement;
use Maell\View\ListComponent\Element;
use Maell\ObjectModel\Property\AbstractProperty;
use Maell\ObjectModel\DataObject;
use Maell\ObjectModel\ObjectUri;
use Maell\ObjectModel\Collection\StatsCollection;
use Maell\ObjectModel\Property\ArrayProperty;
use Maell\ObjectModel\BaseObject;

/**
 * List view object default Web Decorator
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebTab extends WebDefault {

	
    public function render()
    {
    	View::addCoreLib(array('style.css','buttons.css','sprites.css'));
    	View::addCoreLib(array('core.js','view.js','view:alert.js'));
    	
    	// @todo temp fix, sorting by headers is only available on page refresh
    	if ($this->getParameter('paginator') == false) {
    		$this->setParameter('sortable', false);
    	}
    	
    	// set relevant uri adapter and get some identifiers
    	/* @var $_uriAdapter Maell\View\ViewUri\AbstractAdapter */
    	if (! ViewUri::getUriAdapter() instanceof ViewUri\Adapter\GetAdapter ) {
    		$tmp = explode('?', $_SERVER['REQUEST_URI']);
    		$this->_uriAdapter = new ViewUri\Adapter\GetAdapter($tmp[0]);
    	} else {
    		$this->_uriAdapter = ViewUri::getUriAdapter();
    	}
    	
    	$this->_offsetIdentifier	= $this->_uriAdapter->getIdentifier('offset');
    	$this->_sortIdentifier		= $this->_uriAdapter->getIdentifier('sort');
    	$this->_searchIdentifier	= $this->_uriAdapter->getIdentifier('search');
    	
    	$this->_env = $this->_uriAdapter->getEnv();

        if (! $this->_obj->getCollection() instanceof StatsCollection) {
	    	$this->_obj->query($this->_uriAdapter);
        }
            
        $tmp = $this->_obj->reduce();
        $this->_uuid = $tmp['uuid'];
        
        $p = '';
        $p .= sprintf('<div id="comp_%s" class="maell component"><table class="maell list" id="%s">', $this->getId(), $this->getId());
        $p .= $this->_headlineRendering();
		$p .= $this->_contentRendering();
        $p .= '</table>';

        // inject extra content
        $p .= '<div class="actions">' . parent::_contentRendering() . '</div>';
       	$p .= '</div>';
        	 
        return $p;
    }
}
