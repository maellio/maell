<?php

namespace Maell\View\ListComponent\Element\ColumnElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\FormComponent\Element\MediaElement;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebMedia extends AbstractWebDecorator {

	
	public function render()
	{
		if (! is_null($uri = $this->_obj->getValue())) {
			return sprintf('<a href="%s" target="_blank">%s</a>'
				, MediaElement::getDownloadUrl($uri)
				, 'Télécharger'
				);
		} else {
			return 'Pas de fichier lié';
		}
	}
}
