<?php

namespace Maell\View\ListComponent\Element;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\Property\AbstractProperty;

use Maell\ObjectModel\Property\CurrencyProperty;

use Maell\ObjectModel\Property\CollectionProperty;

use Maell\ObjectModel\DataObject;

use Maell\View\ListComponent\Element\AbstractElement;

/**
 * Maell Data Object handling a set of properties tied to an object
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class MetaElement extends AbstractElement {

	
	public function getValue(DataObject $do)
	{
		$property = $do->getProperty($this->getParameter('property'));
		if ($property instanceof CollectionProperty) {
			$subParts = explode('.', $this->getParameter('action'));
			$collection = $property->getValue();
			return $collection->{$subParts[0]}(isset($subParts[1]) ? $subParts[1] : null);
		} else if ($property instanceof AbstractProperty) {
			return $property->getValue();
		} else {
			return $this->_value;
		}
	}
	
	
	public function getDisplayValue(DataObject $do)
	{
		$value = $this->getValue($do);
		switch ($this->getParameter('type')) {
			case 'currency':
				$value = CurrencyProperty::format($value);
				break;
		}
		return $value;
	}
}
