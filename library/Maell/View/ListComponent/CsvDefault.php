<?php

namespace Maell\View\ListComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\ObjectModel\DataObject;
use Maell\ObjectModel\Property\AbstractProperty;
use Maell\View\ViewUri;
use Maell\View\ListComponent\Element;
use Maell\View\Decorator\AbstractCsvDecorator;


/**
 * List view object default Web Decorator
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CsvDefault extends AbstractCsvDecorator {

	
	protected $_instanceof = 'Maell\View\ListComponent';
		
	protected $_offsetIdentifier;
	
	protected $_sortIdentifier;
	
	protected $_searchIdentifier;
	
	
	/**
	 * Array where user parameters can be found (typically $_GET or $_POST)
	 *
	 * @var array
	 */
	protected $_env;
	
	
	/**
	 * Maell\View\ListComponent instance
	 *
	 * @var Maell\View\ListComponent
	 */
	protected $_obj;


	/**
	 * Maell\ObjectModel\Collection instance
	 *
	 * @var Maell\ObjectModel\Collection
	 */
	protected $_collection;
	
	
    public function render()
    {
    	$this->_collection = $this->_obj->getCollection();
    	$this->_collection->setBoundaryBatch(-1);
    	 
        // set relevant uri adapter and get some identifiers
    	/* @var $_uriAdapter Maell\View\ViewUri\AbstractAdapter */
    	if (! ($this->_uriAdapter = ViewUri::getUriAdapter()) instanceof ViewUri\Adapter\GetAdapter ) {
    		$this->_uriAdapter = new ViewUri\Adapter\GetAdapter();
    	}
        $this->_obj->query($this->_uriAdapter);

        $row = '';
        $sep = str_replace('\\t', "\011", ",");
        
        // print out result rows
        foreach ($this->_obj->getCollection()->getMembers(ObjectModel::DATA) as $this->_do) {
        	        
        	$p = null;
        
	        foreach ($this->_obj->getColumns() as $key => $column) {
        		        
        		if ($p) {
        			$p .= $sep;
        		}
        
	            if ($column instanceof Element\IdentifierElement) {
            		$value = $this->_do->getUri()->getIdentifier();
            	} else if ($column instanceof Element\MetaElement) {
            		$value = $column->getDisplayValue($this->_do);
            	} else {
	            	$property = $this->_do->getProperty($column->getParameter('property'));
    	        	$column->setValue($property->getValue());
            	            	 
            		if ($column->getParameter('recursion')) {
						foreach ($column->getParameter('recursion') as $recursion) {
							if ($property instanceof AbstractProperty) {
								$property = $property->getValue(ObjectModel::DATA);
							}
							if ($property instanceof ObjectModel || $property instanceof DataObject) {
								$property = $property->getProperty($recursion);
							}
						}
            		}
            	
            		if ($property instanceof Property\MediaProperty) {
            			$value = '';
            		} else {
	  					$value = ($property instanceof Property\AbstractProperty) ? $property->getDisplayValue() : null;
            		}
            	}
        
        		$fv = str_replace('\r\n', ', ', stripslashes($value));
        		$p .= "\"" . str_replace("\"","\\\"",$fv) . "\"";
	        }
        	$row .= $p . "\r\n";
        	
        	// preserve memory!
        	$this->_do->reclaimMemory();
        }
        
        return $this->_drawHeaderRow() . $row;
    }
    
    
    /**
     * Draw table header
     */
    protected function _drawHeaderRow()
    {
        $row = "";
        $sep = str_replace('\\t', "\011", ",");
        $add_character = "\015\012";
        $add_character = str_replace('\\r', "\015", $add_character);
        $add_character = str_replace('\\n', "\012", $add_character);
        $add_character = str_replace('\\t', "\011", $add_character);
        
        foreach ($this->_obj->getColumns() as $colonne) {
        	if ($row) {
        		$row .= $sep;
        	}
        	$row .= '"' . str_replace("\"","\\\"", $colonne->getTitle()) . '"';
        }
        
        $row .= $add_character;
        //$row = preg_replace("\015(\012)?", "\012", $row);
        
        return $row;
    }
}
