<?php

namespace Maell\View\MenuComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\Core\Layout;

/**
 * Web Decorator for the MenuComponent View Element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	
	
	protected $_currentItem;
	
	
	public function render()
	{
		View::addCoreLib('style.css');
		$html = '';

		if (false !== ($item = $this->_obj->getMenu()->getActiveItem())) {
			$this->_currentItem = $item->getId();
		}	
		foreach ($this->_obj->getMenu()->getItems() as $moduleKey => $module) {
			$menu = '';
			foreach ($module->getItems() as $item) {
				$menu .= $this->_renderMenu($item, $moduleKey);
			}
			
			if ($menu) {
				$label = $this->_escape($module->getLabel());
				if ($moduleKey == Layout::$module) {
					$label = sprintf('<strong>%s</strong>', $label);
				}
				// top level menu
				$html .= sprintf('<ul><li class="head" data-help="%s"><a class="head">%s</a><div class="drop">%s</div></li></ul>'
						, $this->_escape($module->getHelp())
						, $label
						, $menu
				);
			}
		}
	
		return '<div class="maell component menu">' . "\n" . $html . "</div>\n";
	}
	

	protected function _renderMenu($item, $moduleKey)
	{
		$html = ''; $sublevel = false;
		
		$resource = $item->getId();
		if (! $item->fullRes) $resource = $moduleKey . '/' . $resource;
		if (! $item->noLink && (! $this->_obj->isGranted($resource) || $item->hidden)) {
			return '';
		}

		$prefix = sprintf('<li data-help="%s" id="%s">%s</li>' . "\n"
				, $this->_escape($item->getHelp())
				, $this->_makeJsId($item, $moduleKey)
				, $this->_makeLink($item, $moduleKey)
		);
		
		if ($item->getItems()) {
			$sublevel = true;
			foreach ($item->getItems() as $item2) {
				$html .= $this->_renderMenu($item2, $moduleKey);
			}
		}
		
		if ($item->noLink != true) {
			 $html = $prefix;
		}

		if ($sublevel && $html) {
			$html = $prefix . $html;
				
		}
		
		return $html ? '<ul>' . $html . '</ul>' : '';
	}
	
	
	/**
	 * Return a Javascript id build from parameters
	 * @param Maell\Core\Layout\Menu\Item $item
	 * @param string $module
	 * @return string
	 */
	protected function _makeJsId($item, $module = null)
	{
		$rsc = ($item->fullRes != true) ? $module . '/' . $item->getId() : $item->getId(); 
		return 'menu-' . str_replace('/','-', $item->getId());
	}
	
	
	/**
	 * Create a link for given item
	 * @param Maell\Core\Layout\Menu\MenuItem $item
	 * @param string $module
	 * @return string
	 */
	protected function _makeLink($item, $module = null)
	{
		if ($item->fullRes) {
			$module = '/';
		} else if (substr($item->getId(),0,1) == '/') {
			$module = null;
		} else {
			if ($module) $module = '/' . $module . '/';
		}
		
		$label = $this->_escape($item->getLabel());
		
		if ($item->getId() == $this->_currentItem) {
			$label = sprintf('<strong>%s</strong>', $label); 
		}
		
		return sprintf('<a%s>%s</a>'
						, $item->noLink ? null : sprintf(' href="%s%s"', $module, $item->getId())
						, $label
					  );
	}
}
