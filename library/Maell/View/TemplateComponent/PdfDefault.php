<?php

namespace Maell\View\TemplateComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Decorator\AbstractPdfDecorator;

/**
 * Decorator class for template objects in a PDF context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class PdfDefault extends AbstractPdfDecorator {

	
	const TAG_START = "%";
	
	const TAG_END	= "%";
	
	
	protected $_instanceof = 'Maell\View\TemplateComponent';
		
	
	/**
	 * Turn a HTML-based template into a PDF element
	 * 
	 * @param TCPDF $pdf
	 * @param integer $width
	 */
    public function render(\TCPDF $pdf, $width = null)
	{
		$deco = new WebDefault($this->_obj);
		$pdf->writeHTMLCell(null, null, $this->_obj->getParameter('pos_x'), $this->_obj->getParameter('pos_y'), $deco->render());
	}
}
