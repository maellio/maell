<?php

namespace Maell\View\GridComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View;
use Maell\View\ViewUri;
use Maell\View\Decorator;
use Maell\View\FormComponent\Element\ButtonElement;
use Maell\View\ListComponent\Element;
use Maell\ObjectModel\Property\AbstractProperty;
use Maell\ObjectModel\DataObject;
use Maell\ObjectModel\ObjectUri;
use Maell\ObjectModel\Collection\StatsCollection;
use Maell\ObjectModel\Property\ArrayProperty;
use Maell\ObjectModel\BaseObject;

/**
 * List view object default Web Decorator
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	/**
	 * Uri adapter
	 *
	 * @var Maell\View\Uri\AbstractAdapter
	 */
	protected $_uriAdapter;
	
	protected $_offsetIdentifier;
	
	protected $_sortIdentifier;
	
	protected $_searchIdentifier;
	
	protected $_instanceof = 'Maell\View\GridComponent';
	
	/**
	 * Array where user parameters can be found (typically $_GET or $_POST)
	 *
	 * @var array
	 */
	protected $_env;
	
	
	/**
	 * Maell\View\ListComponent instance
	 *
	 * @var Maell\View\ListComponent
	 */
	protected $_obj;

	
	/**
	 * Maell\ObjectModel\Collection instance
	 * 
	 * @var Maell\ObjectModel\Collection
	 */
	protected $_collection;
	
	
	/**
	 * Current data object
	 * @var Maell\ObjectModel\DataObject
	 */
	protected $_do;
	
	protected $_key;
	
	protected $_uuid;
	
	
    public function render()
    {
    	View::addCoreLib(array('style.css','buttons.css','sprites.css'));
    	View::addCoreLib(array('core.js','view.js','view:alert.js', 'view:grid.js'));
    	
    	$reduced = $this->_obj->reduce();
    	
		View::addEvent(sprintf("maell.view.register('%s', new maell.view.grid(%s,'%s'))"
				, 'children' //$this->_obj->getId()
				, \Zend_Json::encode($reduced), 'children'), 'js');
    }

   
    protected function _headerRendering()
    {
		if (trim($this->_obj->getTitle()) == '') {
			$title = 'List';
		} else {
			$title = $this->_escape($this->_obj->getTitle());
		}

		$status = ($this->_obj->getParameter('open_default') == true) ? 'open' : 'close';
    	$html_head = <<<HTML
<div class="{$this->getParameter('css')}" id="{$this->_instanceof}_{$this->_obj->getId()}">
<h4 class="title slide_toggle {$status}"><div class="icon"></div>{$title}</h4>
<div class="content">
HTML;

    	return $html_head;
    }
    
    
    protected function _headlineRendering()
    {
    	$sort = null;
    	
       // build header line
        $line = '<tr>';
        
        $sortIdentifiers = (isset($this->_env[$this->_sortIdentifier]) && is_array($this->_env[$this->_sortIdentifier])) ? $this->_env[$this->_sortIdentifier] : array();
        
        if ($this->_obj->getParameter('selectable') !== false) {
        	$cbid = 'toggler_' . $this->_id;
        	$line .= sprintf('<td><input type="checkbox" id="%s"/></td>', $cbid);
        	
        	View::addEvent(sprintf('maell.view.bindLocal(jQuery("#%s"), "change", function() { t=jQuery("#%s").attr("checked");jQuery("#%s").find(\':checkbox[id=""]\').each(function(i,o){o.checked=t});})'
        					, $cbid
        					, $cbid
        					, $this->_id
        					), 'js');
        }
        
        foreach ($this->_obj->getColumns() as $val) {
        	$line .= sprintf('<th class="tb-%s"><strong>', $val->getId());
        	if ($this->getParameter('sortable') == false) {
        		$line .= $this->_escape($val->getTitle()) . '</strong></th>';
        		continue;
        	}
        	
        	if (is_object($val) && array_key_exists($val->getId(), $sortIdentifiers)) {
        		$by = ($sortIdentifiers[$val->getId()] == 'ASC') ? 'DESC' : 'ASC';
        		
        		// save correct sorting field reference to re-inject in uri after column construction
        		// @todo think twice ! this is crappy as hell!
        		$sort = array($val->getId(), $sortIdentifiers[$val->getId()]);
        		
        	} else {
        		$by = 'ASC';
        	}
        	
            if (is_object($val)) $this->_uriAdapter->setArgument($val->getId(), $by, $this->_sortIdentifier);
            
            $line .= sprintf('<a href="%s">%s</a></strong></th>'
            				, $this->_uriAdapter->makeUri()
            				, $this->_escape($val->getTitle())
            				);
            				
            $this->_uriAdapter->unsetArgument(null, $this->_sortIdentifier);
        }

        if (is_array($sort)) {
            $this->_uriAdapter->setArgument($sort[0], $sort[1], $this->_sortIdentifier);	
        }

        if (count($this->_obj->getEvents('row')) > 0) {
        	$line .= '<th class="tb-actions">&nbsp;</th>';
        } else {
        	$line .= '<th class="tb-actions">&nbsp;</th>';
        }
        
        return $line . "</tr>\n";
    }

       
    protected function _contentRendering()
    {
        $i = 0;
        $p = '';
        
        $aliases = $this->_obj->getAliases();
        
        // print out rows
        foreach ($this->_obj->getCollection()->getMembers(ObjectModel::DATA) as $this->_key => $this->_do) {
        	$css = $i%2 == 0 ? 'odd' : 'even';
        	
        	// @todo handle objects coming from different backends
			$p .= sprintf('<tr data-member="%s" data-alias="%s" class="%s">'
					, $this->_key
					, $aliases[$this->_do->getUri()->getIdentifier()]
					, $css
        		  );
        	$i++;
			
			if ($this->_obj->getParameter('selectable') === true) {
				// make list items selectable
				$p .= sprintf('<td><input type="checkbox" name="maell_selection[]" value="%s"/></td>'
							, $this->_do->getUri()->getIdentifier()
							 );
			}
			
			$altDec = (array) $this->_obj->getParameter('decorators');
            foreach ($this->_obj->getColumns() as $column) {
            	if ($column instanceof Element\IdentifierElement) {
            		$p .= sprintf('<td>%s</td>', $this->_do->getUri()->getIdentifier());
            		continue;
            	}
            	if ($column instanceof Element\MetaElement) {
            		$attrib = ($column->getParameter('type') == 'currency') ? ' class="cellcurrency"' : null;
            		$p .= "<td$attrib>" . $column->getDisplayValue($this->_do) . '</td>';
            		continue;
            	}
            	$property = $this->_do->getProperty($column->getParameter('property'));
            	if (! $property) {
            		$p .= '<td>??</td>';
            		continue;
            	}
            	
            	$column->setValue($property->getValue());
            	
            	/* if a decorator has been declared for property/element, use it */
            	if (isset($altDec[$column->getId()])) {
            		$column->setDecorator($altDec[$column->getId()]);
            		$deco = Decorator::factory($column);
            		$p .= sprintf('<td>%s</td>', $deco->render());
            		continue;
            	}
            	
            	$attrib = sprintf(' class="tb-%s', $column->getId());
            	$attrib .= ($property instanceof Property\CurrencyProperty) ? ' cellcurrency"' : '"';
            	 
            	if ($column->getParameter('recursion')) {
            		$parts = $column->getParameter('recursion');
					foreach ($parts as $rkey => $recursion) {
						
						if ($property instanceof ArrayProperty) {
							// property won't be a property here !
							$property = $property->getValue();
							$property = $property[$recursion];
							if ($property instanceof BaseObject && isset($parts[$rkey+1])) {
								$property = $property->{$parts[$rkey+1]};
							}
							break;
						}
						
						// property is an object property
						if ($property instanceof AbstractProperty && $property->getValue()) {
							$property = $property->getValue(ObjectModel::DATA)->getProperty($recursion);
						}
						
						if ($property instanceof ObjectModel || $property instanceof DataObject) {
							$property = $property->getProperty($recursion);
						} else if ($property instanceof ObjectUri) {
							$property = ObjectModel::factory($property)->getProperty($recursion);
						}
					}
					
					//\Zend_Debug::dump($property->getDisplayValue());
            	}
            	
            	if ($property instanceof Property\MediaProperty) {
            		$column->setValue($property->getDisplayValue());
            		$deco = Decorator::factory($column);
            		$value = $deco->render();
            	} else {
	  				$value = ($property instanceof Property\AbstractProperty) ? $property->getDisplayValue() : $property;
            	}
            	//$p .= "<td$attrib>" . $this->_escape($value) . '</td>';
            	$p .= "<td$attrib>" . $value . '</td>';
            }

            $p .= '<td class="tb-actions">';
            foreach ($this->_obj->getEvents('row') as $button) {
            	$button->setParameter('uri', $this->_do->getUri());
                $p .= $this->_renderButton($button, $aliases[$this->_do->getUri()->getIdentifier()]);
			}
            $p .= '</td></tr>' . "\n";
        }
        
        return $p;
    }
    
    
    protected function _footerRendering()
    {
    	$offset = $this->_obj->getParameter('offset');
    	$batch  = $this->_obj->getParameter('batch');
    	$max    = $this->_obj->getParameter('max');
    	$sbatch = 20;

        if ($max == 0) return '<div class="maell paginator">Aucune fiche</div></div></div>';
    	
        foreach ($this->_env as $key => $val) {
            if (is_array($val)) {
                foreach($val as $key2 => $val2) $this->baseUrl .= $key . '[' . $key2 . ']' . '=' . @rawurlencode($val2) . '&';
            } else if ($key != $this->_offsetIdentifier) {
            	$this->baseUrl .= $key . '=' . rawurlencode($val) . '&';
            }
        }


	   $premier = floor($offset/($batch * $sbatch)) * $sbatch;

	   $i = $j = 0;

	   $p = sprintf('<div class="maell paginator">Fiches %s &agrave; %s sur %s<br/>'
	               , ($offset+1)
	               , ($offset + $batch > $max)
	                 ? $max : ($offset + $batch)
	               , $max
	   			   );

	   // no navigation links needed if total of rows is =< batch value
	   if ($max <= $batch) return $p . '</div></div></div>';

	   // add one page backward link if applicable
	   if ($offset > 0) {
	       $p .= $this->_navigationLink($offset - $batch, '<<');
	   }

	   // jump to minus ($sbatch value) * $batch
       if ($offset >= $sbatch * $batch) {
		  $p .= $this->_navigationLink($offset - ($sbatch * $batch), $sbatch . ' pr&eacute;c.');
	   }

    	while ($i+($premier * $batch) < $max) {
		  $p .= $this->_navigationLink($i + ($premier * $batch), ($premier + $j + 1));
		  $i += $batch;
		  if (++$j > $sbatch-1) break;
	   }

	   // add 'jump 20 pages forward' link if applicable
	   if ($offset + ($sbatch * $batch) < $max) {
	       $p .= $this->_navigationLink($i + ($premier * $batch), $sbatch . ' suiv.');
	   }

	   // add 'jump to next page' link if applicable
	   if ($max > $offset + $batch) {
		  $p .= $this->_navigationLink($offset + $batch, '>>');
	   }

	   return "$p</div></div>";
    }

    
    protected function _navigationLink($offset, $page)
    {
	   if ($offset != $this->_obj->getParameter('offset')) {
	   	  $this->_uriAdapter->setArgument($this->_offsetIdentifier, $offset);
	   	  return sprintf('<a href="%s" class="maell paginator">%s</a> ', $this->_uriAdapter->makeUri(), $page);

	   } else {
		  return '<span class="maell paginator current">' . $this->_escape($page) . '</span> ';
	   }
    }
}
