<?php

namespace Maell\View\SimpleComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Decorator class for component objects in a Web context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	protected $_instanceof = 'Maell\View\SimpleComponent';
	
	
	public function render()
	{
		View::addCoreLib(array('style.css'));
		
		return    $this->_headerRendering()
				. $this->_contentRendering()
				. $this->_footerRendering();
	}
	
	
	protected function _headerRendering()
	{
		$status = $this->_obj->getParameter('open_default') ? 'open' : 'close';
		$status .= $this->_obj->getParameter('locked') ? ' locked' : '';
		$title = $this->_obj->getTitle() ? $this->_escape($this->_obj->getTitle()) : 'Component';
		
		$html = <<<HTML
<div class="{$this->getParameter('css')}" id="{$this->getId()}">
<h4 class="title slide_toggle {$status}"><div class="icon"></div>{$title}</h4>
<div class="content">

HTML;

		return $html;
	}
	
	
	protected function _footerRendering()
	{
		return "</div></div>\n";
	}
}
