<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;

/**
 * Basic class providing a decorator factory.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Decorator {

	
	/**
	 * Array of path where to search for decorators in order
	 * @var array
	 */
	static protected $_paths = array();
	
	
	public static function addPath($path, $ns = 'maell')
	{
		if (is_dir($path)) {
			if (substr($path,-1) != DIRECTORY_SEPARATOR) {
				$path .= DIRECTORY_SEPARATOR;
			}
			array_unshift(self::$_paths, array('path' => $path, 'ns' => $ns));
		} else {
			throw new Exception("'$path' is not a directory");
		}
	}
	
	
	/**
	 * Factory pattern used to instanciate a proper decorator for the given object extended from Maell\View\ViewObject
	 *
	 * @param Maell\View\ViewObject $object disabled for now because of legacy problems
	 * @param array $params
	 * @return Maell\View\AbstractDecorator
	 */
	public static function factory($object, array $params = null)
	{
		$fullclassname = '';
    	$decoratorData = $object->getDecorator();
    	if (is_null($params) && isset($decoratorData['params'])) {
    		$params = $decoratorData['params'];
    	} elseif (!is_null($params) && isset($decoratorData['params'])) {
    		$params = array_merge($params, $decoratorData['params']);
    	}
    	
    	// class name without the first component of the namespace
    	$class = substr(get_class($object), strpos(get_class($object),'\\')+1);
    	
    	if (View::getViewType() != Adapter\WebAdapter::ID) {
	    	$decoratorData['name'] = 'Default';
    	}
    	
    	if (! isset($decoratorData['name']) || trim($decoratorData['name']) == '') {
	    	$decoratorData['name'] = 'Default';
    	}
    	
    	// decorator name
    	$deconame = View::getContext() . ucfirst($decoratorData['name']);
    	foreach (self::$_paths as $library) {
    		$file = $library['path'] . str_replace('\\','/', $class) . '/' . $deconame . '.php';
    		if (file_exists($file)) {
	    		require_once $file;
	    		$fullclassname = '\\' . $library['ns'] . '\\' . $class . '\\' . $deconame;
	    		if (class_exists($fullclassname)) {
	    		    try {
    					$decorator = new $fullclassname($object, $params);
			    	} catch (Exception $e) {
    					throw new Exception("Error instanciating '$fullclassname' decorator.", $e->getCode(), $e);
    				}
    				return $decorator;
	    		}
	    	}
    	}
    	throw new Exception("The decorator class '$fullclassname' doesn't exist or was not found");
	}
}
