<?php

namespace Maell\View\MapComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell;
use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Web Decorator for the MapComponent View Element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	
	
	public function render()
	{
		View::addRequiredLib("https://maps.googleapis.com/maps/api/js?key=AIzaSyBqd7DhiXy1Qgai1pfG_vq3oJ9vSX4ujQI", 'js');
		View::addCoreLib('view:map.js');
		
		$options = array('zoom' => $this->_obj->getParameter('zoom'));
		
		//if (! $this->_obj->getId()) {
			$this->_obj->setId('comp_' . substr(md5(microtime(true)), 0, 8));
		//}
		
		View::addEvent(sprintf("maell.view.register('%s', new maell.view.map(%s,%s))"
				, $this->_obj->getId()
				, \Zend_Json::encode($this->_obj->reduce())
				, \Zend_Json::encode($options)
		), 'js');
	
		return sprintf('<div id="%s" class="maell component map"></div>', $this->_obj->getId());
	}
}
