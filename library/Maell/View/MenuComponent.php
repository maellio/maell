<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Layout;
use Maell\Core\Acl;


class MenuComponent extends ViewObject {
	
	
	protected $_menu;
	
	
	protected $_role;
	
	
	public function setMenu(Layout\Menu $menu)
	{
		$this->_menu = $menu;
		return $this;
	}
	
	
	public function setRole(Acl\Role $role)
	{
		$this->_role = $role;
		return $this;
	}
	
	
	public function isGranted($resource)
	{
		if (! $this->_role) return true;
		
		return $this->_role->granted($resource);
	}
	
	
	public function getMenu()
	{
		return $this->_menu;
		return $this->_role ? $this->_getAclMenu() : $this->_menu;
	}
	
	
	protected function _getAclMenu()
	{
		$menu = clone $this->_menu;
		foreach ($menu->getItems() as $key => $item) {
			
			if (! in_array($key . '/' . $item->getId(), $this->_role->getPrivileges())) {
				
			}
		}
	}
}
