<?php

namespace Maell\View\Action;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Action\AbstractAction;
use Maell\ObjectModel;
use Maell\View\Exception;

/**
 * Class providing an AJAX form controller.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CrudAction extends AbstractAction {

	/**
	 * Object class
	 *
	 * @var string
	 */
	protected $_objClass = 'Maell\ObjectModel\BaseObject';

	
	protected $_obj;
	
	
	protected $_id = 'action';
	
	
	public function setClass($class)
	{
		$this->_class = $class;
		return $this;
	}
	
	
	public function getClass()
	{
		return $this->_class;
	}

	
	/**
	 * Execute the action and returns a result
	 *
	 * @return array
	 */
	public function execute()
	{
		$res = false;
		
		switch ($this->_callback) {
			
			case 'create':
				
				$this->_obj = ObjectModel::factory($this->getClass());
				foreach ($this->getContext() as $key => $value) {
					if ($this->_obj->setProperty($key, $value) === false) {
					    throw new Exception(sprintf("Unable to set '%s' property value", $key));
					}
				}
				$res = $this->_obj->save();
				break;
		}
		return $res;
	}
	
	
	public function getObject()
	{
		return $this->_obj;
	}
}
