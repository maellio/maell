<?php

namespace Maell\View\Action;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use	Maell\Core;
use Maell\View\Action\AbstractAction;

/**
 * Class handling remote actions on objects
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class TestAction extends AbstractAction {

	
	protected $_id = 'object';
	
	
	/**
	 * Execute the action and returns a result
	 *
	 * @return array
	 */
	public function execute(array $data = array())
	{
	}

	
	public function reduce(array $params = array())
	{
		/* keep object in registry */
		$this->setContextData('uuid', Core\Registry::set($this->_obj));
	
		$fullAction = $this->_id;
		if ($this->_action) $fullAction .= '/' . $this->_action;
	
		$array = array(
				'event'		=> 'click', //$this->getParameter('event'),
				'action'	=> $fullAction,
				'data'		=> $this->getContext(),
		);
	
		if ($this->_callbacks) $array['callbacks'] = $this->_callbacks;
	
		// add or replace data with optional $params['data'] content
		if (isset($params['extra'])) {
				
			foreach ((array) $params['extra'] as $key => $val) {
	
				$array[$key] = $val;
			}
		}
	
		// return reduced action without parameters
		return $array;
	}
}
