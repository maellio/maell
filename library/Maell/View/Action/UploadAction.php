<?php

namespace Maell\View\Action;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Class providing an AJAX autocompletion controller.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class UploadAction extends AbstractAction {

	
	protected $_id = 'action/upload';
	
	
	protected $_callbacks = array();
	
	
	protected $_context = array('minChars' => 3, 'displayMode' => 'list', 'defaultSelect' => true);
	
	
	/**
	 * Object class
	 *
	 * @var string
	 */
	protected $_objClass = 'Maell\View\FormComponent\Element\MediaElement';
	
	
	/**
	 * Execute the action and returns a result
	 *
	 * @return array
	 */
	public function execute($params = null)
	{
	}

	
	public function reduce(array $params = array())
	{
		$array = parent::reduce($params);
		
		return $array;
	}
}
