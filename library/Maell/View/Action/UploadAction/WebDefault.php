<?php

namespace Maell\View\Action\UploadAction;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		View::addCoreLib(array('view:action:upload.js'));
		
		$id = $this->_nametoDomId($this->_obj->getId());
		$event = sprintf("maell.view.register('%s', new maell.view.action.uploader(%s,'%s'))"
						, $this->_obj->getId()
						, \Zend_Json::encode($this->_obj->reduce(array('params' => array())))
						, $this->_obj->getObject()->getId()
						);
		View::addEvent($event, 'js');
		View::addEvent(sprintf("maell.view.get('%s').init()", $id), 'js');
	}
}
