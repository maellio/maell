<?php

namespace Maell\View\Action\AutocompleteAction;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use	Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		View::addCoreLib(array('view:table2.js','view:action:autocomplete.js'));
		
		if ($this->_obj->getBoundObject()) {
			$id = $this->_nametoDomId($this->_obj->getBoundObject()->getId());
		} else {
			$id = md5(microtime());
		}
		
		$event = sprintf("maell.view.register('%s', new maell.view.action.autocomplete(%s,'%s'))"
						, $id
						, \Zend_Json::encode($this->_obj->reduce(array('params' => array(), 'collections' => 1)))
						, $id
						);
		View::addEvent($event, 'js');
		View::addEvent(sprintf("maell.view.get('%s').init()", $id), 'js');
	}
}
