<?php

namespace Maell\View\TableComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\Decorator;
use Maell\View\TableComponent;

/**
 * Decorator class for table objects in a Web context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	

	public function render()
	{
		switch ($this->_obj->_disposition) {
							
			case TableComponent::DISP_ROWS:
				$html = $this->_byRowsRendering();
				break;
				
			case TableComponent::DISP_COLS:
				if ($this->getParameter('sortable') === true) {
					
					$params = array('headers' => array());
					
					foreach ($this->_obj->getFields() as $key => $field) {
						//var_dump($field);
						/* we disable sorting on columns where preserveLabel parameter is set to true as they may contain HTML */
						if ($field->getParameter('preserveLabel') === true) {
							
							$params['headers'][$key] = array('sorter' => false);
						}
					}
				}
				$html = $this->_byColsRendering();
				break;
		}
		
		return $this->_headerRendering() . $html . $this->_footerRendering();
	}
	
	protected function _byRowsRendering()
	{
		$colRow = array();
		$html = '';
		if ($this->_obj->getParameter('columns')) {
			
			$html .= sprintf('<div class="%s_lcol">', $this->_cssStyle);
		}
		
		$lines = 0;//$this->_obj->lines();
		$i=0;
		
		foreach($this->_obj->getFields() as $field) {
			
			$colRow[$field->getAltId()] = '';
			
			if ($this->_obj->getParameter('columns') && $i == $lines) {
				$colRow[$field->getAltId()] .= sprintf('</div><div class="%s_rcol">', $this->_cssStyle);
			}
			
			$colRow[$field->getAltId()] .= '<fieldset>' . "\r\n"
										. '<span class="label" style="min-width:85px">' . $this->_escape($field->getTitle()) . '</span>';
			
			foreach($this->_obj->getRows() as $key => $row) {
				if ($row instanceof ObjectModel\DataObject) {
					$colRow[$field->getId()] .= sprintf('&nbsp;<span class="field">%s</span>'
							, $this->_escape($row->getProperty($field->getId())->getDisplayvalue())
					);
				} else {
					$colRow[$field->getId()] .= sprintf('&nbsp;<span class="field">%s</span>'
									 , $field->formatValue($row[$field->getId()])
									);
				}
			}
			
			$colRow[$field->getAltId()] .= '</fieldset>';
			$i++;
		}
		$html .= implode("\n", $colRow);
		return $html;
	}
	
	
	protected function _byColsRendering()
	{
		$body = $head = '';
		
		foreach($this->_obj->getFields() as $field) {
			
			$label = $field->getTitle();
			
			if ($field->getParameter('preserveLabel') == false) {
				
				$label = $this->_escape($label);
			}
			$head .= '<th>' . $label . '</th>';
		}
		
		if (count($this->_obj->getButtons()) > 0) $head .= '<th>&nbsp;</th>';
		
		$head = sprintf('<tr class="">%s</tr>', $head);
		$i=0;
		
		foreach($this->_obj->getRows() as $key => $row) {
			
			$line = '';

			foreach($this->_obj->getFields() as $field) {

				$colId = $field->getAltId();
				$rowId = $this->_obj->getId() . '_' . $key . '_' . $colId;
				
				$line .= sprintf('<td id="%s" %s>%s</td>'
								 , $rowId
								 , ''
								 , $field->formatValue($row[$colId])
								);
			}
			
			if (count($this->_obj->getButtons()) > 0) $line .= $this->_buttonsRendering($row);
			
			$css = $i%2 == 0 ? 'odd' : 'even';
			$body .= '<tr id="' . $this->_obj->getId() . '_' . $key . '" class="' . $css . '">' . $line . '</tr>';
			$i++;
		}
		
		$thead = '<thead>' . $head . '</thead>';
		$tbody = '<tbody>' . $body . '</tbody>';
		
		$class = $this->getParameter('sortable') ? ' class="tablesorter"' : '';
		
		if ($this->getParameter('sortable')) {
			$class = ' class="tablesorter"';
			$info = '<div class="info">Vous pouvez trier les valeurs d\'une colonne en cliquant sur son ent&ecirc;te, et sur plusieurs colonnes avec shift+clic (maj+clic).</div>';
		}
		
		return @$info.'<table class="maell component table">' . $thead . $tbody . '</table>';
	}

	
	protected function _headerRendering()
	{
		$title = $this->_obj->getTitle() ? $this->_obj->getTitle() : 'Table';
		$status = $this->_obj->getParameter('open_default') ? 'open' : 'close';
		$html = <<<HTML
<div class="maell component" id="{$this->_obj->getId()}">
<h4 class="title slide_toggle {$status}"><div class="icon"></div>{$title}</h4>
<div class="content">
HTML;
		return $html;
	}

	
	protected function _footerRendering()
	{
		return '</div></div>' . "\r\n";
	}


	protected function _buttonsRendering(array $data)
	{
		$line = '<td id="buttons">';
		foreach ($this->_obj->getButtons() as $button) {
			$deco = Decorator::factory($button);
			$deco->setParameter('data', $data);
			$line .= $deco->render();
		}
		$line .= '</td>';

		return $line;
	}
}
