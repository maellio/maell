<?php

namespace Maell\View\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\Core\Parameter as Param;
use Maell\Parameter;


/**
 * Class providing the view engine with a PDF-context adapter.
 * For more details on the PDF file format, see adobe.org
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class PdfAdapter extends AbstractAdapter {


	const ID = 'Pdf';
	
	const ORIENTATION_PORTRAIT = 'P';
	
	const ORIENTATION_LANDSCAPE = 'L';
	
	
	const ALIGN_LEFT = 'L';
	
	const ALIGN_CENTER = 'C';
	
	const ALIGN_RIGHT = 'R';
	
	const FORMAT_A4 = 'A4';
	
	const FORMAT_A3	= 'A3';

	
	protected $_context = 'Pdf';
	
	protected $_width;
	
	protected $_document;
	
	
	public function __construct(array $parameters = null)
	{
		error_reporting(0);
		if (!defined('K_PATH_IMAGES')) {
			define ('K_PATH_IMAGES','');
		}
		
		$params = array();
		
		$params['format']	 	= new Param(Parameter::STRING, self::FORMAT_A4, false, array(self::FORMAT_A3, self::FORMAT_A4));
		$params['orientation'] 	= new Param(Parameter::STRING, self::ORIENTATION_PORTRAIT, false, array(self::ORIENTATION_PORTRAIT, self::ORIENTATION_LANDSCAPE));
		$params['destination']	= new Param(Parameter::STRING, 'D');
		$params['copies']		= new Param(Parameter::INTEGER, 1);
		
		$params['fontSize']		= new Param(Parameter::ANY, 9);
		$params['fontName'] 	= new Param(Parameter::STRING, 'Helvetica');
		$params['title'] 		= new Param(Parameter::STRING);
		$params['author'] 		= new Param(Parameter::STRING);
		$params['bookmarks']	= new Param(Parameter::BOOLEAN, false);
		
		$params['logo'] 		= new Param(Parameter::STRING);
		
		$params['fillColor']	= new Param(Parameter::INTEGER, 220);
		$params['drawColor']	= new Param(Parameter::INTEGER, 0);

		$params['tableBorders']	= new Param(Parameter::STRING, '1 1 1 1');
		
		$params['headerMargin'] = new Param(Parameter::INTEGER, 10);
		$params['headerFont']	= new Param(Parameter::STRING);
		$params['headerFontSize']	= new Param(Parameter::INTEGER);
		
		$params['footerMargin'] = new Param(Parameter::INTEGER, 10);
		$params['footerFont']	= new Param(Parameter::STRING);
		$params['footerFontSize']	= new Param(Parameter::INTEGER);
		
		$params['marginTop']	= new Param(Parameter::INTEGER, 20);
		$params['marginLeft']	= new Param(Parameter::INTEGER, 20);
		$params['marginRight']	= new Param(Parameter::INTEGER, 20);
		$params['marginBottom']	= new Param(Parameter::INTEGER, 20);
		
		$params['addHeader']	= new Param(Parameter::BOOLEAN, true);
		$params['addFooter']	= new Param(Parameter::BOOLEAN, true);
		
		$params['autoPageBreak']	= new Param(Parameter::BOOLEAN, true); 
		
		$this->_setParameterObjects($params);
		
		parent::__construct($parameters);
	}
	
	
    public function display()
    {
    	require_once 'vendor/tcpdf/tcpdf/tcpdf.php';
        $this->_document = new \TCPDF($this->getParameter('orientation'), 'mm', $this->getParameter('format'), true); 
     //   $this->_document->setImageScale(1/28);
        $this->_document->setPrintHeader($this->getParameter('addHeader'));
        $this->_document->setPrintFooter($this->getParameter('addFooter'));
        
        // set document information
        $this->_document->SetCreator('Maell using ' . get_class($this->_document));
        
        $this->_document->SetAuthor($this->getParameter('author'));

        $this->_document->SetTitle($this->getParameter('title'));
        $this->_document->SetSubject($this->getParameter('title'));

        //set auto page breaks
        $this->_document->SetAutoPageBreak($this->getParameter('autoPageBreak'), $this->getParameter('marginBottom'));
        
        // header and footer declaration
        if ($this->getParameter('headerMargin') > 0) {
        	
        	if ($this->getParameter('logo')) {
        		$imgformat = getimagesize($this->getParameter('logo'));
        		//\Zend_Debug::dump($imgformat);
        		$this->setParameter('marginTop', 50);// (int) round($this->getParameter('marginTop') + $imgformat[1]/7));
	       		$this->setParameter('headerMargin', 50); //(int) ($this->getParameter('marginTop') + ($imgformat[1]/7)));
        		$this->_document->setHeaderData($this->getParameter('logo'), $imgformat[0]/4);
        	} else {
            
	        	$this->_document->setHeaderMargin($this->getParameter('headerMargin'));
    	    	$this->_document->setHeaderData('','', utf8_encode($this->getParameter('title')));
        	}
        }
        
        $this->_document->SetFooterMargin($this->getParameter('footerMargin'));

        //set margins
        $this->_document->SetMargins( $this->getParameter('marginLeft')
        		, $this->getParameter('marginTop')
        		, $this->getParameter('marginRight')
        );
        
        
        // font definition
        if ($this->getParameter('fontName')) {
        	
	        $this->_document->setHeaderFont(array($this->getParameter('fontName'), ''
	        									, $this->getParameter('headerFontSize') ? $this->getParameter('headerFontSize') : $this->getParameter('fontSize')-2));
    	    $this->_document->setFooterFont(array($this->getParameter('fontName'), ''
    	    									, $this->getParameter('footerFontSize') ? $this->getParameter('footerFontSize') : $this->getParameter('fontSize')-2));
        	$this->_document->SetFont($this->getParameter('fontName'));
        }
        
        // colors definition
        $this->_document->SetFillColor($this->getParameter('fillColor'));
        $this->_document->SetDrawColor($this->getParameter('drawColor'));
        
        $this->_document->setLanguageArray(array(	'a_meta_charset'  => 'UTF-8',
                                            		'a_meta_dir'      => 'ltr',
                                            		'a_meta_language' => 'fr',
                                            		'w_page'          => 'page')
                                     );
                                     
        // default display size is 100%                             
        $this->_document->SetDisplayMode(100);

        // largeur utile de la page
        $this->_width = $this->_document->getPageWidth() - (PDF_MARGIN_LEFT + PDF_MARGIN_RIGHT);
        
        $this->_document->SetFontSize($this->getParameter('fontSize'));
        
        //initialize document
        $this->_document->AliasNbPages();
        $this->_document->AddPage();    
        
        if ($this->_template) {

        	// document mode
        	$this->_();
        	
        } else {
        	
        	// registered objects rendering mode
        	$this->_render();
        }
        
        // duplicate pages x times if parameter is set to
        if ($this->getParameter('copies') > 1) {
        	$total = $this->_document->getNumPages()+1;
        	for ($i = 1 ; $i < $this->getParameter('copies') ; $i++) {
        		for ($j = 1 ; $j < $total ; $j++) {
        			$this->_document->copyPage($j);
        		}
        	}
        }
        
        
        $doc = $this->getParameter('title') ? str_replace('/', '-', $this->getParameter('title')) . '.pdf' : 'Export.pdf';
        return $this->_document->Output($doc, $this->getParameter('destination'));
    }

    
    protected function _render()
    {
		$content = null;
		$newpage = false;
    	$elems = View::getObjects(View::PH_DEFAULT);
    	
    	if (is_array($elems)) {
    		foreach ($elems as $key => $elem) {
				$object = $elem[0];
    			$params = $elem[1];

    			if ($this->getParameter('bookmarks') === true && ($key == 0 || $object->getParameter('newpage') == 'after') && $object->getTitle()) {
	    			$this->_document->Bookmark($object->getTitle());
	    		}
	    		
    			$params['borders'] = $this->getParameter('tableBorders');
    			
    			try {
   					$decorator = \Maell\View\Decorator::factory($object, $params);
    				} catch (Exception $e) {
    					$this->_document->Write($e->getMessage());
    				}
    		    			
    			// check wether we need to jump to a new page
    			if (in_array($decorator->getParameter('newpage'), array('before','both')) || $newpage === true) {
    				
	    				$this->_document->AddPage();
	    				if ($this->getParameter('bookmarks') === true) {
	    					$this->_document->Bookmark($object->getTitle());
	    				}
	    				$newpage = false;
    			}
    			
    			$decorator->render($this->_document, $this->_width);
 			
    		    if (in_array($decorator->getParameter('newpage'), array('after','both'))) {
    		    	// new page will be added only if it is necessary (other view elements to print)
    				$newpage = true;
    			} else {
	    			// new line
    				$this->_document->Ln();
    			}
    		}
    	}
    }
    
    
    protected function _()
    {
    	$template = file_get_contents($this->_template);
   	
    	// transform some characters
    	$template = str_replace("\t", str_repeat("&nbsp;", 15), $template);
    	$template = str_replace("\n", "<br/>", $template);
    	
    	$tagPattern = "/%([a-z0-9]+)\\:([a-z0-9.]*)\\{*([a-zA-Z0-9:,\\\"']*)\\}*%/";
    	
    	$tags = array();
    	
    	preg_match_all($tagPattern, $template, $tags, PREG_SET_ORDER);
    	
    	// PHASE 1
    	foreach ($tags as $key => $tag) {
    		
    		$content = '';
    		
    		if($tag[1] == 'env') {
    			$content = \Maell::htmlEncode(\Maell\View::getEnvData($tag[2]));
    			unset($tag[$key]);
    		}
    		
       		if (isset($tag[0])) {
       			$template = str_replace($tag[0], $content, $template);
       		}
    	}
    	
    	$this->_document->writeHTML($template);
    	
    	// PHASE 2 - other tags
        	foreach ($tags as $tag) {
    		
    		$content = '';
    		
    		switch ($tag[1]) {
    			
    			case 'container':
    				
    				$elems = \Maell\View::getObjects($tag[2]);
    				
    				if (is_array($elems)) {
    					foreach ($elems as $elem) {
    						$object = $elem[0];
    						$params = $elem[1];
    						
    						if (! is_object($object)) continue;
    						switch (get_class($object)) {
    				
    							case 'Maell\View\ViewObject':
    								$decorator = \Maell\View\Decorator::factory($object, $params);
    								$decorator->render($this->_document, $this->_width);
    								break;
    					
    							default:
    								break;
    						}
    					}
    				}
    				break;
    		}
    	}
    }
    
    
    public function addPage()
    {
        $this->_document->AddPage(); 
    }
    
    
    public function setTitle($title)
    {
    	$this->setParameter('title', $title);
    }
    
    
    public function isNewPage()
    {
    	return ($this->_document->getY() == $this->getParameter('marginTop')); 
    }
}
