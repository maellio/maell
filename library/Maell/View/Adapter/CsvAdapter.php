<?php

namespace Maell\View\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View,
	Maell\View\Decorator;
use Maell\Core\Parameter;

/**
 * Class providing the view engine with a CSV-context adapter.
 * Adaptateur Csv pour le moteur de vue
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CsvAdapter extends AbstractAdapter {

	
	const ID = 'Csv';
	
	
	protected $_context = 'Csv';
	
	protected $_allowedEvents = array();
	
	protected $_componentsBasePath;
	
	
	public function __construct(array $parameters = null)
	{
		$params = array();
		$params['title'] = new Parameter('string');
		$this->_setParameterObjects($params);
		parent::__construct($parameters);
	}

    
    public function display()
    {
    	$filename = $this->getParameter('title') ? $this->getParameter('title') : 'Export';
    	$filename .= '.csv';
    	
        header("Content-type: text/x-csv");
        header("Content-Disposition: attachment; filename=\"$filename\"");
    	return $this->_render();
    }

    
    protected function _render()
    {
		$content = null;    	
    	$elems = View::getObjects(View::PH_DEFAULT);
    	
    	if (is_array($elems)) {
    		foreach ($elems as $elem) {
				$object = $elem[0];
    			$params = $elem[1];
    			    					
    			if (! is_object($object)) continue;
    			
    			/* @var $object Maell\View\ViewObject */ 
    			switch (get_class($object)) {
    				
    				case 'Maell\View\ListComponent':
    					$object->setDecorator();
    					$decorator = Decorator::factory($object);
    					$content .= $decorator->render();
    					break;
    					
    				default:
    					break;
    			}
    		}
    	}
    	return $content;
    }
}
