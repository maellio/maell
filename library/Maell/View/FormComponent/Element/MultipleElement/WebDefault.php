<?php

namespace Maell\View\FormComponent\Element\MultipleElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 876 $
 */

use Maell\View,
	Maell\View\ViewUri,
	Maell\View\Decorator\AbstractWebDecorator;

/**
 * Maell default web decorator for enum elements
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		// set correct name for field name value depending on 'mode' parameter value
		$name = $this->_obj->getId();
		
		if ($this->getParameter('mode') == View\FormComponent::SEARCH_MODE) {
			$name = ViewUri::getUriAdapter()->getIdentifier('search') . '[' . $name . ']';
			$this->setParameter('radiomax',0);
		}
		
		$html = '';
		foreach ($this->_obj->getEnumValues() as $key => $val) {
			$uniqid = sprintf('%s_%s', $name, $key);
			$html .= sprintf('<input type="checkbox" name="%s[]" id="%s" value="%s"%s/>&nbsp;<label for="%s">%s</label> '
							, $name
							, $uniqid
							, $key
							, in_array($key, $this->_obj->getValue()) ? ' checked="checked"': null
							, $uniqid
							, $this->_escape($val)
							);
		}
		return $html;
	}
}
