<?php

namespace Maell\View\FormComponent\Element\GridElement;

use Maell\View\ListComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use	Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\Decorator;
use Maell\View\GridComponent;
use Maell\View\FormComponent\Element\ListElement;
use Maell\View\Action\AutocompleteAction;

/**
 * Maell default web decorator for list elements
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		$render = '';
		$co = $this->_obj->getCollection();
		
		// if sorting is enabled, change collection parameter to sort on defined sorting property
		if ($this->_obj->getParameter('enablesort') == true) {
			$co->resetSortings();
			$co->setSorting(array($this->_obj->getParameter('sortingprop')));
		}
		
		// rendering is basically a list and an optional form (put on top or bottom)
		$list = new GridComponent($co, array('columns' => explode(',', $this->_obj->getParameter('display'))));

		if ($this->_obj->getParameter('enableupdate')) {
			$list->addRowAction('%grid.update%', 'Update');
		}
		
		if ($this->_obj->getParameter('enabledelete')) {
			$list->addRowAction('%grid.delete%', 'Delete');
		}

		if ($this->_obj->getParameter('enablecreate')) {
			$ac = new AutocompleteAction(clone $co);
			$deco = Decorator::factory($ac);
			$render = $deco->render();
		}
		
		$list->setParameter('uricache', false);
		$list->setTitle($this->_obj->getTitle());
		$deco = Decorator::factory($list, array('paginator' => false));
		return $render . $deco->render();
	}
}
