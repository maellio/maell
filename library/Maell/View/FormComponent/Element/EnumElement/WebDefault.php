<?php

namespace Maell\View\FormComponent\Element\EnumElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View,
	Maell\View\ViewUri,
	Maell\View\Decorator\AbstractWebDecorator;

/**
 * Maell default web decorator for enum elements
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		// set correct name for field name value depending on 'mode' parameter value
		$name = $this->_obj->getId();
		
		if ($this->getParameter('mode') == View\FormComponent::SEARCH_MODE) {
			$name = ViewUri::getUriAdapter()->getIdentifier('search') . '[' . $name . ']';
			$this->setParameter('radiomax',0);
		}

		if (count($this->_obj->getEnumValues()) > $this->getParameter('radiomax')) {
			// display menu list
			$options = array(null => $this->getParameter('defaultlabel')) + (array) $this->_obj->getEnumValues();
			$zv = new \Zend_View();
			return $zv->formSelect($name, $this->_obj->getValue(), null, $options);
		
		} else {
			$html = '';
			foreach ($this->_obj->getEnumValues() as $key => $val) {
				$html .= sprintf('<input type="radio" name="%s" id="%s_%s" value="%s"%s/>&nbsp;<label for="%s_%s">%s</label> '
								, $name
								, $name
								, $key
								, $key
								, ($key == $this->_obj->getValue()) ? ' checked="checked"': null
								, $name
								, $key
								, $this->_escape($val)
								);
			}
			return $html;
		}
	}
}
