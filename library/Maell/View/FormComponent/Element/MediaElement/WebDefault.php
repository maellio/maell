<?php

namespace Maell\View\FormComponent\Element\MediaElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\Action\UploadAction;
use Maell\View;
use Maell\View\FormComponent\Element\MediaElement;
use Maell\Core;

/**
 * Maell default web decorator for list elements
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		// set correct name for field name value depending on 'mode' parameter value
		$name = $this->_obj->getId();
		$prefix = \Maell::getController('medias');
		if (! $prefix) {
			$prefix = '/maell/medias/';
		}
		$uri = $prefix . 'upload';
		View::addCoreLib(array('core.js','locale.js','view.js','uploader.css','view:action:upload.js'));
		View::addEvent(sprintf("maell.view.register('%s_ul', new maell.view.action.upload(jQuery('#%s_ul'),'%s'))", $name, $name, $uri), 'js');
		
		$html = '';
		// @todo code media deletion JS
		if (($this->_obj->getValue()) != null) {
			$html .= sprintf('<span><a href="%s" target="_blank">%s %s</a> | <a href="#" onclick="maell.view.get(\'%s_ul\').reset(this)">%s</a></span>'
					, MediaElement::getDownloadUrl($this->_obj->getValue()->getUri())
					, 'Télécharger'
					, $this->_obj->getValue()->getLabel()
					, $name
					, 'Supprimer'
			);
		}
		$html .= sprintf('<div id="%s_ul" class="qq-upload-list"></div>', $this->_nametoDomId($name));
		$html .= sprintf('<input type="hidden" name="%s" id="%s" value="%s" class="hiddenfilename"/>'
				, $name
				, $this->_nametoDomId($name)
				, $this->_obj->getValue() ? $this->_obj->getValue()->getIdentifier() : null
		);
		
		return $html;
		
		$action = new UploadAction($this->_obj);
		
		$deco = View\Decorator::factory($action);
		$deco->render();
		
		return $html . "\n";
	}
}
