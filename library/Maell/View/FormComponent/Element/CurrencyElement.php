<?php


namespace Maell\View\FormComponent\Element;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\FormComponent\Element\AbstractElement;

/**
 * Simple form currency element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CurrencyElement extends AbstractElement {

	
	public function formatValue($str = null)
	{
		 if (! is_null($str)) {

		 	setlocale(LC_MONETARY, 'fr_FR.UTF-8');
			return money_format('%n', $str);
			
		} else {
			
			return $str;
		}
	}
}
