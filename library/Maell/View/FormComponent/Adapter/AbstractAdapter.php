<?php

namespace Maell\View\FormComponent\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\ObjectModel;
use Maell\ObjectModel\ObjectUri;
use Maell\ObjectModel\Property;
use Maell\View\FormComponent\Element\FieldElement;
use Maell\ObjectModel\Property\AbstractProperty;

/**
 * Maell View Form Adapter Abstract class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
abstract class AbstractAdapter implements AdapterInterface {

	
	protected $_elements = array();
	
	
	protected $_buttons = array();
	
	
	protected $_data = array();
	
	
	protected $_errors = array();
	

	public function build(ObjectModel\DataObject $do, array $display = null, $identifier = false) {
		
		if ($identifier === true) {
			$identifier = new FieldElement(ObjectUri::IDENTIFIER);
			$identifier->setTitle("Identifiant unique")
						 ->setConstraint(Property::CONSTRAINT_MANDATORY, true)
						   ->setConstraint(Property::CONSTRAINT_MAXLENGTH, 10);
			$this->addElement($identifier);
		}
		
		if (is_null($display)) {
			$display = array_keys($do->getProperties());
		}
		
		foreach ($display as $element) {
			$property = $do->getRecursiveProperty($element);
			if ($property instanceof AbstractProperty) {
				/* convert property to form element */
				$this->addElementFromProperty($property, $element, (count($this->_elements)+1) * 100);
			}
		}
	}
		
	
	public function validate()
	{
		return true;
	}
	
	
	public function addElementFromProperty(Property\AbstractProperty $property, $position = null)
	{
		throw new View\Exception("You need to redeclare this function in your adapter to use it");
	}
	
	
	public function addElement($element, $position = null)
	{
		$this->_elements[$element->getId()]  = $element;
		return $this;
	}
	
	
	public function getElements()
	{
		return $this->_elements;
	}
	
	
	public function getElement($key)
	{
		return (isset($this->_elements[$key])) ? $this->_elements[$key] : false; 
	}
	

	public function getButton($key)
	{
		return (isset($this->_buttons[$key])) ? $this->_buttons[$key] : false; 
	}
	
	
	public function getErrors()
	{
		return $this->_errors;
	}
	
	
	public function position($key)
	{
		return isset($this->_positions[$key]) ? $this->_positions[$key] : false;
	}
	
	
	public function keepOnly($var)
	{
		if (! is_array($var)) $var = (array) $var;
		
		foreach ($this->_elements as $key => $element) {
			if (! in_array($element->getId(), $var)) {
				unset($this->_elements[$key]);
			}
		}
		return $this;
	}
}
