<?php

namespace Maell\View\FormComponent\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;

/**
 * Maell View Form Adapter interface
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
interface AdapterInterface {

	
	public function build(ObjectModel\DataObject $do, array $display = null);
	
	public function addElementFromProperty(Property\AbstractProperty $property, $position);
	
	public function addElement($element, $position);
	
	public function validate();
	
	public function getElement($key);
	
	public function getButton($key);
	
	public function getErrors();
	
	public function keepOnly($var);
	
	public function position($key);
}
