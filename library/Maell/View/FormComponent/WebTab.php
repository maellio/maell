<?php

namespace Maell\View\FormComponent;


use Maell\View\FormComponent\Element;

class WebTab extends WebView {
   
    
    protected function _contentRendering()
    {
		$p = '<fieldset>';
		$altDecorators = (array) $this->_obj->getParameter('decorators');
		
        foreach ($this->_obj->getColumns() as $key => $element) {
        	
        	$field = $element;
        	
        	if ($this->getParameter('hideempty') === true && ! $field->getValue()) {
        		continue;
        	}
        	
        	/* hidden fields treatment */
        	if ($element->getConstraint(Element\AbstractElement::CONSTRAINT_HIDDEN) === true) {
        		continue;
        	}
        	
        	$label = '&nbsp;';
	        $label = $this->_escape($element->getTitle());

    	    $line = sprintf('<div class="clear"></div><div class="label"><label for="%s" data-help="%s">%s</label></div>'
    	    				, $field->getAltId()
    	    				, $field->getHelp()
            				, $label
            			 );
            $p .= $line . '<div class="field">' . $field->formatValue($field->getValue()) . '</div>';
        }
        $p .= "</fieldset>\n";
        return $p;
    }


    protected function _headerRendering()
    {
		return sprintf('<div class="%s" id="comp_%s"><form>', $this->getParameter('css'), $this->_obj->getId());
    }

    
    protected function _footerRendering()
    {
    	return '</form></div>';
    }
}
