<?php

namespace Maell\View\TabsComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\Decorator;

/**
 * Decorator class for component objects in a Web context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	protected $_instanceof = 'Maell\View\TabsComponent';
	
	
	public function render()
	{
		if (! $this->_obj->getId()) {
			$this->_obj->setId('comp_' . substr(md5(microtime(true)), 0, 8));
		}
		
		View::addVendorLib('components/jqueryui/jquery-ui.min.js');
		View::addVendorLib('components/jqueryui/themes/ui-lightness/jquery-ui.min.css');
		View::addCoreLib('style.css');
		View::addEvent(sprintf('jQuery("#%s").tabs()', $this->_obj->getId()), 'js');
		return parent::render();
	}
	
	
	protected function _contentRendering()
	{
		$html = '';
		foreach ($this->_obj->getContent() as $component) {
			$component->setDecorator('tab');
			$deco = Decorator::factory($component);
			$html .= $deco->render();
		}
		return $html;
	}
	
	
	protected function _headerRendering()
	{
		$number = 1;
		$html = sprintf('<div id="%s" class="maell tabs"><ul>', $this->_obj->getId());
		foreach ($this->_obj->getContent() as $component) {
			$component->setId(sprintf('tabs-%d', $number++));
			$html .= sprintf('<li><a href="#comp_%s">%s</a></li>', $component->getId(), $component->getTitle());
		}
		return $html . '</ul>';
	}
	
	
	protected function _footerRendering()
	{
		return "</div>\n";
	}
}
