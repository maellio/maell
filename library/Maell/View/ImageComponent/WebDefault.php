<?php

namespace Maell\View\ImageComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Decorator class for image objects in a Web context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	protected $_obj;

	
	protected $_instanceof = 'Maell\View\ImageComponent';
	
	
	public function render()
	{
		return sprintf('<img src="%s" alt="%s" title="%s" />', $this->_obj->getContent(), $this->_obj->getTitle(), $this->_obj->getTitle());
	}
}