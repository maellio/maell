<?php

namespace Maell\View\ImageComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core;
use Maell\View\Decorator\AbstractPdfDecorator;

/**
 * Decorator class for image objects in a PDF context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class PdfDefault extends AbstractPdfDecorator {
	
	
	protected $_instanceof = 'Maell\View\ImageComponent';
	
		
    public function render(\TCPDF $pdf, $width = null)
	{
		$image = $this->_obj->getContent();
		$imgSize = getimagesize(\Maell::$basePath . $image);
			
		$height = $pdf->pixelsToUnits($imgSize[1]);
		$width  = $pdf->pixelsToUnits($imgSize[0]);
		
		$pdf->setJPEGQuality(75);
		
		if ($this->_obj->getParameter('pos_x') || $this->_obj->getParameter('pos_y')) {
			$currentY = $pdf->getY();
			$currentAPB = $pdf->getAutoPageBreak();
			$currentMB = $pdf->getMargins();
			$currentMB = $currentMB['bottom'];
			$pdf->setAutoPageBreak(false, 0);
			$pdf->write('');
		}
		
		$pdf->Image(  \Maell::$basePath . $image
					, $this->_obj->getParameter('pos_x') ? $pdf->pixelsToUnits($this->_obj->getParameter('pos_x')) : null
					, $this->_obj->getParameter('pos_y') ? $pdf->pixelsToUnits($this->_obj->getParameter('pos_y')) : null
					, $width * $this->_obj->getParameter('ratio')
					, $height * $this->_obj->getParameter('ratio')
					, null // img type
					, $this->_obj->getParameter('link')
					, 'N'
				  );
		
		if (isset($currentY)) {
			$pdf->setY($currentY);
			$pdf->setAutoPageBreak($currentAPB, $currentMB);
		}
	}
}
